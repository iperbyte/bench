#! /usr/bin/env python3
import hashlib
import sys
arg=0
if len(sys.argv)>=2:
    arg=str(sys.argv[1])
str1 = b"""1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890XCVB"""*5000
omd5=hashlib.md5()
osha1=hashlib.sha1()
osha224=hashlib.sha224()
osha256=hashlib.sha256()
osha384=hashlib.sha384()
osha512=hashlib.sha512()
cicli=500
ciclieffettuati=0
for x in range(0, cicli):
    ciclieffettuati +=1
    omd5=hashlib.md5()
    osha1=hashlib.sha1()
    osha224=hashlib.sha224()
    osha256=hashlib.sha256()
    osha384=hashlib.sha384()
    osha512=hashlib.sha512()
    omd5.update(str1)
    osha1.update(str1)
    osha224.update(str1)
    osha256.update(str1)
    osha384.update(str1)
    osha512.update(str1)
    strmd5=omd5.hexdigest()
    strsha1=osha1.hexdigest()
    strsha224=osha224.hexdigest()
    strsha256=osha256.hexdigest()
    strsha384=osha384.hexdigest()
    strsha512=osha512.hexdigest()
print ("Hasing Test Python (md5 sha1,sha224,sha246,sha284,sha512)- Serie %s Cicli %d,Lunghezza Stringa %d"%(arg,ciclieffettuati,len(str1)))
#print("MD5   "+strmd5)
#print("SH1   "+strsha1)
#print("SH224 "+strsha224)
#print("SH256 "+strsha256)
#print("SH348 "+strsha384)
#print("SH512 "+strsha512)
