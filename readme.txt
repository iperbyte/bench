Il software su questo repository è da considerarsi sotto GPL
Scritto a cazzo da Claudio Bertuzzi

########## Prerequisiti ##########à
   1) lanciare il file testinit.sh che carica i programmi necessari Elencati sotto (Eventualmente modificarlo per eliminare quelli facoltativi):

     INDISPENSABILE TEST HANDBRAKE (magari nella forma non daily): ppa:stebbins/handbrake-snapshots ## PROBABILMENTE E' MEGLIO METTERE QUELLA non DAILY
     NON SERVE PER IL BENCH: ppa:motumedia/mplayer-daily #mplayer non indispensabile, ma se si vuole vedere il video può fare comodo
     NON SERVE PER IL BENCH: ppa:smplayer2/daily #smplayer non indispensabile, ma se si vuole vedere il video può fare comodo

     NON SERVE PER IL BENCH: git git-gui #Se si è già scaricato questo script con git (sarà già presente) NON SERVE PER IL BENCH
     NON SERVE PER IL BENCH: chromium-browser firefox webaccounts-chromium-extension unity-chromium-extension ttf-lyx NON SERVE PER IL BENCH ma internet fa sempre comodo 
     NON SERVE PER IL BENCH: aptitude (ma per me è parte della roba di sistema)
     INDISPENSABILE TEST PYRIT: pyrit
     NON SERVE PER IL BENCH: geany geany-common geany-plugins geany-plugins-common geany-plugin-gdb geany-plugin-addons NON SERVE, ma lo trovo un ottimo editor per correggere errori in questo codice
     INDISPENSABILE PER TEST IN PARALLELO: parallel NOTA, di parallel ne esiste un altra versione, quella buona e più evoluta è quella nel pacchetto parallel
     INDISPENSABILE TEST HANDBRAKE(handbrake, e presumo le librerie di contorno) ffmpeg h264enc mencoder handbrake-gtk handbrake-cli x264 
     FACOLTATIVO SPECIE KRUSADER SE IL DESKTOP NON E' KDE mplayer2 smplayer2 krusader #non indispensabile
     INDISPENSABILE TEST DI COMPRESSIONE E SYSBENCH: p7zip-full lrzip pigz lbzip2 sysbench  
     INDISPENSABILE PER LETTURA HARDWARE, e TEST HARDINFO (A PARTE CREDO cpufrequtils): hwinfo hardinfo x86info cpufrequtils 
     INDISPENSABILI: xterm leafpad  
   
   2) lanciare ./turbobost.sh Che mette su turbostat. Serve per il test, serve anche se la macchina non dovesse essere compatibile.

########## PER FARE PARTIRE IL BENCH #################
sudo ./bench.sh   #PER ORA NON SONO PREVISTE OPZIONI

gli altri file:
 testinit.sh #mette su i prerequisiti
 turboboost.sh #sistema la questione turbostat.
 send.sh #non centra serve per mandare le modifiche al codice sul repository GIT (per poter contribuire e avere accesso in scrittura mandare un email di richiaesa a claudio@iperbyte.it)

########## TEST EFFETTUATI ##################
Test Effettuati
   sysbench CON --cpu-max-prime=50000 e --num-threads=NUMERO DI THREAD DISPONIBILI PER IL PROCESSORE
   hasing in pyton (sequenziale 10 iterazioni e parallelo 100 iterazioni) ? Per il parallelo Valutare di fare un numero di operazioni multiplo dei core e di "noramlizzare il risultato"
   hasing in perl  (sequenziale 10 iterazioni e parallelo 100 iterazioni) ? Per il parallelo Valutare di fare un numero di operazioni multiplo dei core e di "noramlizzare il risultato"
       Non ho fatto un md5 reale su un file perchè è troppo dipendente dal disco (e per ora è un test di processore)   
   AUTOTEST di 7zip
   7zip (multithread)
   lrzip (multithread)
   gzip (signlethread)
   pigz (multithread e vincitore alla grande pere fare dei copatibili .gz) 
   bzip (signlethread)
   lbzip (multithread)
      non ho fatto dei tar.gz o .bz perchè il tar aggiunge solo lo strato di raggruppamento dei file (bassissimo impegno per il processore, solo flusso disco), per il test ho preferito tenere solo la parte di compressione che è quella che impegna il processore
   pyrit Effettuato nella versione long
   hardinfo NOTA questo test a seconda dell'hardware puo andare in segfault. nel test viene lanciato finchè non riesce (per un massimo di 10 volte) e se non riesce mai viene indicato come fallito



######## CONTENUTO DEL FILE DI REPORT
vedi file dentro dir del test "~/.config/bcbench/repo/[DATA]_[ORA.MILIARDESIMI]_result.txt"

    Nel report (ancora da normalizzare come output per farlo parsare semplicemente ad un prog esterno che ne utilizza i valori)
    sono inseriti:
    1) i risultati dei test
    2) i valori di turbostat (se disponibile) 
       valori minimi a riposo
       valori massimi con un singolo core a tavoletta
       valori massimi con un tutti i core a tavoletta
    3) info sulla cpu
    4) info sulla ram
    5) info sulla vga
    6) info sulla scheda madre
    7) info su (x)buntu
       aggiungere le versioni dei programmi di test e se si riesce a capire delle librerie utilizzate
    ULTIMO -1) VERSIONE DEI TEST
    ULTIMO -0) UUIDTEST... In identificativo per identificare in modo univoco un singolo test  (sereve ad un eventuale gestore esterno per non inserire due volte in un DB lo stesso test)   


############ OUTPUT ############

la directory di output è nella propria home
~/.config/bcbench  # Qua ci dovrebbe stare il file di configurazione, ma non ho ancora fatto il prog di gestione

~/.config/bcbench/data
	Contiene il video per i test che viene scaricato al primo utilizzo.
	Nota: se lo si è gia scaricato una volta e non si vuole riscaricarlo, per ora non c'è un sistema "noramle".
	      Consiglio di lanciare il test e interromperlo con ctrl+c quando parte wget.
	      Poi copiare il video dentro a ~/.config/bcbench/data sovrascrivendo quello parzialmente scaricato da wget
	      NOTA di programmazione. Interrompendo wget il video viene scaricato solo a meta, ma visto che ha il nome corretto, il resto del programma non se ne accorge e lo considera valido (anche se probabilmente fallirà handbrake)
	      (magari verificarne dimensioni e un md5 sarebbe una bella idea)

~/.config/bcbench/tmp
	Contiene lo "Sporco prodotto da ogni elaborazione, viene riscritto alla successive" Per ora ho scelto di non cancellare ne i video ne gli zip prodotti
	Nota i .sh sono script che vengono lanciati attraverso xterm perchè in linea avevo difficolta (o una sintassi troppo complessa) ad esplodere le variabili o a fare un redirect dell'output... 	
	
	gli xterm.*.log sono gli output catturati dal logging di xterm (che risulta piu efficace di un normale redirect, specie per programmi bastardi come turbostat o 7z)
		è utilizzato per estrapolare i dati e utile a scopo di debug se qualcosa va storto).

~/.config/bcbench/repo
	Contiene il risultato del test nel file: "~/.config/bcbench/repo/[DATA]_[ORA.MILIARDESIMI]_result.txt"
	Altri file importanti sono...
	~/.config/bcbench/repo/[DATA]_[ORA.MILIARDESIMI]_benchresult.txt"  RISULTATO DEI BENCH (utilizzato da cat per produrre il report finale)
	~/.config/bcbench/repo/[DATA]_[ORA.MILIARDESIMI]_turbostat.log"    INFORMAZIONI TURBOSTAT (utilizzato da cat per produrre il report finale)
	~/.config/bcbench/repo/[DATA]_[ORA.MILIARDESIMI]_HARDWARE.log"     INFORMAZIONI HARDWARE (utilizzato da cat per produrre il report finale)
	
	
	Gli altri file nella stessa data contengono le informazioni grezze. Diversamente da quelli dentro /tmp, non vengono utilizzati nemmeno durante l'elaborazione. 
	E potranno venire eliminati in una versione più definitiva. Hanno unicamente uno scopo di DEBUG
	
################ TODO #####################
1) FATTO! IMPORTANTE Aggiungere in parallelo monitor memoria per verificare che non ci sia swap e, se c'è, segnalarlo nei risultati dei test
2) (sistemato per monitor memoria e info cpu ) normalizzare l'output nella forma :: @nomecampo = valore
3) Se trovo la maniera di caricare i driver corretti in base all'hardware (nvidia,ati,intel)  per un avvio da usb esterno con hardware che cambia ogni volta
   aggiungere un pyrit su scheda video
4) Verificare l'opzione FAKETEST (per verificare solo formalmente l'output del test in maniera rapida)
5) Gestire un .conf dentro alla directory ~/.config/bcbench che contenga l'impostazione di quali test eseguire
6) Gestire le opzioni di lancio. Almeno INIT e DOWNLOAD. La prima crea l'ambiente senza eseguire il test e la seconda scarica il video ... Magari con download=/media/miakeyusb... Cerca il file sulla miakeyusb
      #NOTA che mi ero preso.... Aggiungere un opzione INIT che crea la dir, e DOWNLOAD che scarica il file video di riferimento, Magari un CLEAN che la pulisce e DELETEALL che la elimina, compreso il video)
7) info minispeedtest su hdd in uso. Anche se il test per ora non è indirizzato all'HDD è ovio che la velocità dell'hdd influisce sia in compressione che in codifica video.
8) Aggiungere su ultimo dist-upgrade,moduli e versione handbrake ed eventuali librerie video che utilizza (anche se non ho idea di quali siano), versione 7zip e altri compressori,versione python3 e perl e se si riesce a reperirle su librerie di hasing.
9) Pulire e correggere la parte descrittiva (prima dei valori) dell'output [DATA]_[ORA.MILIARDESIMI]_result.txt e indicare MEGLIO ALTO/MEGLIO BASSO