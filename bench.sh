#!/bin/bash

# Versione 0.1_prealfa
 
SEQUENCE_TEST_VERSION="0.0.prealfa"
OUTPUT_VERSION="0.3.prealfa"

#Info utili
# redirect:  http://www.linuxjournal.com/content/bash-redirections-using-exec
#Verifica lancio da root

#exit code
NOROOT=1
PROCESSORIMISTI=2 #PER ORA IL BATCH GESTISCE SOLO CORE TUTTI UGUALI

# DirChiamante, Dir Script, dir ConfigScript ,DataUsataneneinomifile,  User e group di chi ha chiamato il sudo (tutti i file saranno assegnati a questo utente e questo gruppo)
CALLDIR=$(pwd) #Directory Di lancio dello script
SCRIPTDIR=$( dirname "${BASH_SOURCE[0]}" ) #Directory dello script (dentro cui ci sono anche i bench)
BENCHDIR="${HOME}/.config/bcbench" #Directory home dello script (contiene il mov e i risultati)
LOGDATE=`date -u +"%Y-%m-%d_%H-%M-%S.%N"` #data
SUDO_GRP=`id -g $SUDO_USER` #Gruppo dell'utente che ha chiamato il sudo
UUID_IDTEST=$(uuidgen)
YELLOW="\033[1;33m"
RED="\033[0;31m"
ENDCOLOR="\033[0m"
if [ $USER != root ]; then
  echo -e $RED"Error: must be root"
  echo -e $YELLOW"Exiting..."$ENDCOLOR
  exit $NOROOT
fi

#E' il numero piu affidabile di tread che ho trovato. dmidecode --type processor non ci prende sempre
INFO_TOT_THREAD=$(grep -e "^processor\\s*:\\s\+[[:digit:]]*" < /proc/cpuinfo |wc -l)


echo "Benchmark Start."
echo "Lasciami stare, ho mal di testa."
#lbzip2

#lrzip -L 9 -N -20 -T -f -L 9    -o tmp/prova_lzh.lrz data/ToS-4k-1920.mov
#lrzip -L 9 -N -20 -T -f -L 9 -g -o tmp/prova_gzip.lrz data/ToS-4k-1920.mov
#lrzip -L 9 -N -20 -T -f -L 9 -b -o tmp/prova_bzip.lrz data/ToS-4k-1920.mov
#lrzip -N -20 -T -f -L 9 -z -o tmp/prova_QZIP.lrz data/ToS-4k-1920.mov
#gzip -9 -c data/ToS-4k-1920.mov >tmp/gzip_prova.gz
#pigz -9 -k -c data/ToS-4k-1920.mov >tmp/pigz_prova.gz
#bzip2 -9 -c data/ToS-4k-1920.mov >tmp/bzip2_prova.gz
#lbzip2 -9 -c data/ToS-4k-1920.mov >tmp/bzip2_prova.gz
#pbzip2 -9 -c data/ToS-4k-1920.mov >tmp/pbzip2_prova.gz

source $(dirname $0)/benchtest/bench_setting.sh
source $(dirname $0)/benchtest/bench_createenvdir.sh
source $(dirname $0)/benchtest/bench_lib.sh
source $(dirname $0)/benchtest/bench_hardinfo.sh
source $(dirname $0)/benchtest/bench_infoturbostat.sh
source $(dirname $0)/benchtest/bench_md5.sh
source $(dirname $0)/benchtest/bench_comp.sh
source $(dirname $0)/benchtest/bench_handbrake.sh
source $(dirname $0)/benchtest/bench_pyrit.sh
source $(dirname $0)/benchtest/bench_sysbench.sh
source $(dirname $0)/benchtest/bench_workmem.sh


HARDINFO_TEST_RIUSCITO=0

leggiturbobost
prepare_hardinfo_value
start_md5benc
prepare_hardinfo_value
start_handbraketest
start_zipcomp
prepare_hardinfo_value
start_pyrit_test
prepare_hardinfo_value
start_sysbench_test
prepare_hardinfo_value
write_hardinfo_test_value

ferma_monitor_memoria
evaluatememeworking

echo " " >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
echo " " >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
echo "HARDWARE:" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
cat "${BENCHDIR}/repo/${LOGDATE}_benchresult.txt" "${BENCHDIR}/repo/${LOGDATE}_freemem.log" "${BENCHDIR}/repo/${LOGDATE}_turbostat.log" "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" > "${BENCHDIR}/repo/${LOGDATE}_result.txt"
echo " " >> "${BENCHDIR}/repo/${LOGDATE}_result.txt"
echo " " >> "${BENCHDIR}/repo/${LOGDATE}_result.txt"
echo "SEQUENCE TEST VERSION - :: @SEQUENCE_TEST_VERSION = $SEQUENCE_TEST_VERSION" >> "${BENCHDIR}/repo/${LOGDATE}_result.txt"
echo "OUTPUT VERSION        - :: @OUTPUT_VERSION = $OUTPUT_VERSION" >> "${BENCHDIR}/repo/${LOGDATE}_result.txt"
echo "ID TEST               - :: @UUID_ID_TEST = $UUID_IDTEST" >> "${BENCHDIR}/repo/${LOGDATE}_result.txt"
nohup leafpad "${BENCHDIR}/repo/${LOGDATE}_result.txt" &
echo "${LOGDATE}" >>"${BENCHDIR}/tmp/last_test"
echo FINITO

