YELLOW="\033[1;33m"
RED="\033[0;31m"
ENDCOLOR="\033[0m"
if [ $USER != root ]; then
  echo -e $RED"Error: must be root"
  echo -e $YELLOW"Exiting..."$ENDCOLOR
  exit 0
fi


apt-get install acpidump linux-tools linux-tools-common 
#Questa installa linux tools col numero di kernel attuale
apt-get install linux-tools-`uname -r|sed "s'\(^[1234567890.]*\)\([-][[:digit:]]\+\)*\(.*$\)'\1\2'g"`

# Questa aggiunge a /etc/modules la linea msr solo se non esiste
# opzione 1: grep -q -e "^\\S*msr\\S*$" /etc/modules || echo "msr" >>/etc/modules   ## inizia con 0 o più spazi contiene msr e termina con 0 o più spazi
# opzione 2: grep -q -e "\\bmsr\\b" /etc/modules || echo "msr" >>/etc/modules  ## coniene una parola distina msr
# opzione 3: grep -q -e "\\bmsr\\b" /etc/modules || echo "msr" >>/etc/modules  ##
# 1 una regexp ^=inizia -- \\s=con 0 o piu spazi -- msr -- \\s=con 0 o piu spazi --$=termina
# 2 coniene una parola distina msr \\b è il delimitatore di parola
# 3 sfrutta l'opzione di grep -w  per trovare una parola distinta
# Nota!!! la regexp vuole un singolo \s o \b l'altra barra e per maskcare la barra sulla shell
grep -q -e "^\\S*msr\\S*$" /etc/modules || echo "msr" >>/etc/modules


modprobe msr
echo "per vedere lo stato dei processori digita: turbostat"