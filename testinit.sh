sudo add-apt-repository -y ppa:stebbins/handbrake-snapshots
sudo add-apt-repository -y ppa:motumedia/mplayer-daily #mplayer non indispensabile
sudo add-apt-repository -y ppa:smplayer2/daily #smplayer non indispensabile
sudo apt-get update
sudo apt-get install -y mesa-utils
sudo apt-get install -y git git-gui 
sudo apt-get install -y chromium-browser firefox webaccounts-chromium-extension unity-chromium-extension ttf-lyx
sudo apt-get install -y aptitude 
sudo apt-get install -y gawk
sudo apt-get install -y pyrit
sudo apt-get install -y geany geany-common geany-plugins geany-plugins-common geany-plugin-gdb geany-plugin-addons
sudo apt-get install -y parallel
sudo apt-get install -y ffmpeg h264enc mencoder handbrake-gtk handbrake-cli x264 
sudo apt-get install -y mplayer2 smplayer2 krusader #non indispensabile
sudo apt-get install -y p7zip-full lrzip pigz lbzip2 sysbench
sudo apt-get install -y hwinfo hardinfo x86info cpufrequtils 
sudo apt-get install -y xterm leafpad
sudo apt-get -y dist-upgrade 

