
function zipcomp {
    xterm -l -lf "${BENCHDIR}/tmp/xterm.7zip.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 `eval echo $SETTEZIP_PRESET` 1>>"${BENCHDIR}"/tmp/bench.log
}

#xterm -l -lf "${BENCHDIR}/tmp/xterm.7zip.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 `eval echo $SETTEZIP_PRESET`
#lrzip -L 9 -N -20 -T -f -L 9 -b -o tmp/prova_bzip.lrz data/ToS-4k-1920.mov

function lrzipcomp {
    #-fa curier -fs 20 -bg black -fg white
    #2>&1 | tee file.txt
    #
    #SETTEZIP_PRESET
    #echo `eval echo $SETLRZIP_PRESET`
    xterm -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 `eval echo $SETLRZIP_PRESET`
    #local pidterm=$!
    #kill -9 $pidterm
    echo "Compressione con lrzip"
}

function altricomp {
    nice -n -20 `eval echo $1`
}

function zipcompmenob {
    xterm -l -lf "${BENCHDIR}/tmp/xterm.7zip_b.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 7z b
   
    #Esempio di out
    #22:    7950   286   2706   7734  |   101479   363   2524   9155˰
    #23:    8854   317   2841   9021  |    98781   355   2549   9039˰
    #24:    8202   307   2875   8819  |    94345   343   2549   8753˰
    #25:    8397   330   2909   9587  |    99681   370   2535   9374˰
    #----------------------------------------------------------------˰
    #Avr:          310   2833   8790               358   2539   9080˰
    #Tot:          334   2686   8935˰
    
    local rgxp_22="^22:\s+([1234567890]*)\s+.*:\s+([1234567890]*)\s+.*$"
    local rgxp_23="^23:\s+([1234567890]*)\s+.*:\s+([1234567890]*)\s+.*$"
    local rgxp_24="^24:\s+([1234567890]*)\s+.*:\s+([1234567890]*)\s+.*$"
    local rgxp_25="^25:\s+([1234567890]*)\s+.*:\s+([1234567890]*)\s+.*$"
    local rgxp_Avr="^Avr:\s+[1234567890]*\s+([1234567890]*)\s+([1234567890]*)\s+[1234567890]*\s+([1234567890]*)\s+([1234567890]*).*$"
    local rgxp_Tot="^Tot:\s+[1234567890]*\s+([1234567890]*)\s+([1234567890]*).*$"
    
        while read -r line; do
            line=$(echo $line|tr "|" ":") #trasformo il pipe in : perche nella regexp non ho trovato come si maska
            #echo $line
            if [[ "$line" =~ $rgxp_22 ]] ; then
                VAL_7ZIP22a="${BASH_REMATCH[1]}"
                VAL_7ZIP22b="${BASH_REMATCH[2]}"
                echo "$VAL_7ZIP22a $VAL_7ZIP22b"
            elif [[ "$line" =~ $rgxp_23 ]] ; then
                VAL_7ZIP23a="${BASH_REMATCH[1]}"
                VAL_7ZIP23b="${BASH_REMATCH[2]}"
                echo "$VAL_7ZIP23a $VAL_7ZIP23b"
            elif [[ "$line" =~ $rgxp_24 ]] ; then
                VAL_7ZIP24a="${BASH_REMATCH[1]}"
                VAL_7ZIP24b="${BASH_REMATCH[2]}"
                echo "$VAL_7ZIP24a $VAL_7ZIP24b"
            elif [[ "$line" =~ $rgxp_25 ]] ; then
                VAL_7ZIP25a="${BASH_REMATCH[1]}"
                VAL_7ZIP25b="${BASH_REMATCH[2]}"
                echo "$VAL_7ZIP25a $VAL_7ZIP25b"
            elif [[ "$line" =~ $rgxp_Avr ]] ; then
                VAL_AVRSC="${BASH_REMATCH[1]}"
                VAL_AVRMC="${BASH_REMATCH[2]}"
                VAL_AVRSD="${BASH_REMATCH[3]}"
                VAL_AVRMD="${BASH_REMATCH[4]}"
                echo "$VAL_AVRSC $VAL_AVRMC $VAL_AVRSD $VAL_AVRMD"
            elif [[ "$line" =~ $rgxp_Tot ]] ; then
                VAL_TOTS="${BASH_REMATCH[1]}"
                VAL_TOTM="${BASH_REMATCH[2]}"
                echo "$VAL_TOTS $VAL_TOTM"
            fi
        done < "${BENCHDIR}/tmp/xterm.7zip_b.log"    
        MBSCOMP=$(echo "scale=2;($VAL_7ZIP22a+$VAL_7ZIP23a+$VAL_7ZIP24a+$VAL_7ZIP25a)/4" |bc)
        echo aa$MBSCOMP
        echo "---" $VAL_7ZIP22b $VAL_7ZIP23b $VAL_7ZIP24b $VAL_7ZIP25b
        MBSDECOMP=$(echo "scale=2;($VAL_7ZIP22b+$VAL_7ZIP23b+$VAL_7ZIP24b+$VAL_7ZIP25b)/4" |bc)
        echo bb$MBSDECOMP
        
        echo "P7ZIP Compressione MB/Sec                                             (alto  è meglio) :: @b7zMBSCOMP      = $MBSCOMP" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        echo "P7ZIP Compressione MIBS per SingoloCore                               (alto  è meglio) :: @b7zVAL_AVRSC    = $VAL_AVRSC" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        echo "P7ZIP Compressione MIBS Complessivo su Tutti i Core                   (alto  è meglio) :: @b7zVAL_AVRMC    = $VAL_AVRMC" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        echo "P7ZIP DEcompressione MB/Sec                                           (alto  è meglio) :: @b7zMBSDECOMP    = $MBSDECOMP" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        echo "P7ZIP DEcompressione MIBS per SingoloCore                             (alto  è meglio) :: @b7zVAL_AVRSD    = $VAL_AVRSD" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        echo "P7ZIP DEcompressione MIBS Complessivo su Tutti i Core                 (alto  è meglio) :: @b7zVAL_AVRMD    = $VAL_AVRMD" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        echo "P7ZIP Media Comp/Decomp MIBS per SingoloCore                          (alto  è meglio) :: @b7zVAL_TOTS     = $VAL_TOTS" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        echo "P7ZIP Media Comp/Decomp MIBS Complessivo su Tutti i Core              (alto  è meglio) :: @b7zVAL_TOTM     = $VAL_TOTM" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        
    
    
    #local pidterm=$!
    #kill -9 $pidterm
    
}

#echo pippo 2
#echo pluto 3

function start_zipcomp {
    prepare_hardinfo_value
    if [ $P7ZIP_test -ne 0 ]; then
        
        svuotamemoria "autotest_7z"
        echo "Autotest 7zip"
        if [ $P7ZIP_bench -ne 0 ]; then
            zipcompmenob
        else
            echo "skip autotest 7zip"
        fi

        prepare_hardinfo_value
        if [ $P7ZIP_Standard -ne 0 ]; then
            svuotamemoria "7z_Compression"
            echo "compressione con 7zip"
            singletest zipcomp
            echo "7zip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_z9.7z     (basso è meglio) = $singletest_dft"
            echo "7zip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_z9.7z     (basso è meglio) :: @test7zip        = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else
            echo "skip test 7zip"
        fi
        
                
        prepare_hardinfo_value
        if [ $LRZIP_test -ne 0 ]; then
            svuotamemoria "lrzip_Compression"
            echo "compressione con lrzip"
            singletest lrzipcomp
            echo "lrzip:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920.lrz       (basso è meglio) = $singletest_dft"
            echo "lrzip:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920.lrz       (basso è meglio) :: @test_7zip       = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else
            echo "skip test lrzip"
        fi
        
        prepare_hardinfo_value
        if [ $GZIP_test -ne 0 ]; then
            svuotamemoria "gzip_Compression"
            echo "compressione con gzip"
            singletest altricomp "${BENCHDIR}/tmp/bench_gzip.sh"
            echo "gzip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_gzip.gz   (basso è meglio) = $singletest_dft"
            echo "gzip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_gzip.gz   (basso è meglio) :: @test_gzip       = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else
            echo "skip test gzip"
        fi
        
        prepare_hardinfo_value
        if [ $PIGZ_test -ne 0 ]; then
            svuotamemoria "pigz_Compression"
            echo "compressione con pigz (gzip compatibile multitread)"
            singletest altricomp "${BENCHDIR}/tmp/bench_pigz.sh"
            echo "pigz:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_pigz.gz   (basso è meglio) = $singletest_dft"
            echo "pigz:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_pigz.gz   (basso è meglio) :: @test_pigz       = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else
            echo "skip test pigz"
        fi
        
        prepare_hardinfo_value
        if [ $BZIP2_test -ne 0 ]; then
            svuotamemoria "bzip2_Compression"
            echo "compressione con bzip2"
            singletest altricomp "${BENCHDIR}/tmp/bench_bzip2.sh"
            echo "bzip2:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) = $singletest_dft"
            echo "bzip2:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_bzip2      = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else
            echo "skip test bzip2"
        fi
        
        if [ $LBZIP2_test -ne 0 ]; then
            prepare_hardinfo_value
            svuotamemoria "lbzip2_Compression"
            echo "compressione con lbzip2 (bzip2 compatibile multitread)"
            singletest altricomp "${BENCHDIR}/tmp/bench_lbzip2.sh"
            echo "lbzip2: tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) = $singletest_dft"
            echo "lbzip2: tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_lbzip2     = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else
            echo "skip test lbzip2"
        fi
    else
        echo "skip ALL P7ZIP_test"
        prepare_hardinfo_value
        prepare_hardinfo_value
        prepare_hardinfo_value
    fi
}
 
