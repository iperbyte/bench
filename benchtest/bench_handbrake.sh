#!/bin/bash

function handbrake {
    #echo nice -n -20 "${BENCHDIR}/tmp/handb.sh" 2>/dev/null 1>>"${BENCHDIR}"/tmp/bench.log
    nice -n -20 "${BENCHDIR}/tmp/handb.sh" 2>/dev/null 1>>"${BENCHDIR}"/tmp/bench.log
}

#echo pippo 1
#echo pluto 2

function start_handbraketest {
    echo "Test elaborazione video con handbrake"
    if [ $HANDB_test -ne 0 ]; then
        if [ $HANDB_Test_x264_mp4 -ne 0 ]; then
            svuotamemoria "Handbrake_Test"
            echo "Test HandBrakeCLI"
            singletest handbrake 
            echo "HandBrakeCLI tempo per convertire ToS-4k-1920.mov in HANDB_x264.mp4   (basso è meglio) = $singletest_dft"
            echo "HandBrakeCLI tempo per convertire ToS-4k-1920.mov in HANDB_x264.mp4   (basso è meglio) :: @handbrakeclim4v = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else 
            echo "SKIP test HandBrakeCLI"
            echo "HandBrakeCLI tempo per convertire ToS-4k-1920.mov in HANDB_x264.mp4   (basso è meglio) :: @handbrakeclim4v = SKIPPED" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        fi
        #eliminato perchè praticamente doppione di quello sopra. (a cambiare è il muxing, e quello sopra lavora meglio in multitread)
        #if [ $HANDB_Test_x264_mkv -ne 0 ]; then
        #    singletest handbrake "HANDB_x264_mkv" "HANDB_x264.mkv"
        #else 
        #    echo "skip test HANDBRAKE_x264_mkv"
        #fi
    fi
}
 
