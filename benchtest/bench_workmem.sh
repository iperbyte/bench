
function getmeminfoduringtest {
gawk '
    BEGIN {
        minmemtot=9999999999999
        minmemused=9999999999999
        minmemfree=9999999999999
        maxmemtot=0
        maxmemused=0
        maxmemfree=0
        minswptot=9999999999999
        minswpused=9999999999999
        minswpfree=9999999999999
        maxswptot=0
        maxswpused=0
        maxswpfree=0
    }
        
    ( /^Mem:/ ) {
        memtot=$2
        memused=$3
        memfree=$4
        #print "MEM",NR,memtot,memused,memfree
        
        if ( memtot < minmemtot )   { minmemtot = memtot}
        if ( memused < minmemused ) { minmemused = memused}
        if ( memfree < minmemfree ) { minmemfree = memfree}
        if ( memtot > maxmemtot )   { maxmemtot = memtot}
        if ( memused > maxmemused ) { maxmemused = memused}
        if ( memfree > maxmemfree ) { maxmemfree = memfree}
    }
    ( /^Swap:/ ) {
        swptot=$2
        swpused=$3
        swpfree=$4
        #print "SWP",NR,memtot,memused,memfree
        if ( swptot  < minswptot  ) { minswptot = swptot}
        if ( swpused < minswpused ) { minswpused = swpused}
        if ( swpfree < minswpfree ) { minswpfree = swpfree}
        if ( swptot  > maxswptot  ) { maxswptot = swptot}
        if ( swpused > maxswpused ) { maxswpused = swpused}
        if ( swpfree > maxswpfree ) { maxswpfree = swpfree}
        memusata=maxmemused-minmemused
        swpusata=maxswpused-minswpused
        totusata=memusata+swpusata
        warnig_swap = "PASS" 
        if ( swpusata>0 ) {warnig_swap = "WARNING"}
    }
    END { print "mMT",minmemtot,"mMU",minmemused,"mMF",minmemfree,"MMT",maxmemtot,"MMU",maxmemused,"MMF",maxmemfree,"mST",minswptot,"mSU",minswpused,"mSF",minswpfree,"MST",maxswptot,"MSU",maxswpused,"MSF",maxswpfree,"MU",memusata,"SU",swpusata,"TU",totusata,"WS",warnig_swap}
  ' "$1"
}

function setglobwarning {
    if [ "$1" == "WARNING" ]; then
        mem_GLOBAL_SWAP_WARNING="WARNING" 
    fi
}

function evaluatememeworking {
echo "Legenda sigle" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mMT = Minima Memoria Totale (salvo scherzi della scheda video dovrebbe essere uguale alla massima)"  >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mMU = Minima Memoria Usata" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mMF = Minima Memoria Libera" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "MMT = Massima Memoria Totale (salvo scherzi della scheda video dovrebbe essere uguale alla minima)"  >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "MMU = Massima Memoria Usata" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "MMF = Massima Memoria Libera" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mST = Minima Swap Totale (se qualcosa di esterno al programma non cambia la swap questo numero non puù cambiare e deve essere uguale alla massima" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mSU = Minima Swap Usata" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mSF = Minima Swap Libera" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "MST = Massima Swap Totale (se qualcosa di esterno al programma non cambia la swap questo numero non puù cambiare e deve essere uguale alla minima" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "MSU = Massima Swap Usata" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "MSF = Massima Swap Libera" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "MU = Memoria usata durante il test (memoriausatamassima-memoriausataminima)" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "SU = SWAP usata durante il test (swapusatamassima-swapusataminima) SE QUESTO VALORE E' DIVERSO DA ZERO HA SWAPPATPO" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "TU = TOT memoria e swap usata per il test"
echo "WS = Segnale di warning swap"
echo " "
mem_sysbench=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_sysbench.log")
mem_md5_single_py=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_benchmd5_py_seriale.log")
mem_md5_multi_py=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_benchmd5_py_multithread.log")
mem_md5_single_pl=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_benchmd5_pl_seriale.log")
mem_md5_multi_pl=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_benchmd5_pl_multithread.log")
mem_handbrake=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_Handbrake_Test.log")
mem_7z_autotest=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_autotest_7z.log")
mem_7z_compression=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_7z_Compression.log")
mem_lrzip_compression=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_lrzip_Compression.log")
mem_gzip_compression=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_gzip_Compression.log")
mem_pigz_compression=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_pigz_Compression.log")
mem_bzip2_compression=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_bzip2_Compression.log")
mem_lbzip2_compression=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_lbzip2_Compression.log")
mem_pyrit=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_pyrit.log")
mem_hardinfo=$(getmeminfoduringtest "${BENCHDIR}/tmp/mem_monitor_hardinfo.log")

echo "mem_sysbench          = $mem_sysbench" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_md5_single_py     = $mem_md5_single_py" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_md5_multi_py      = $mem_md5_multi_py" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_md5_single_pl     = $mem_md5_single_pl" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_md5_multi_pl      = $mem_md5_multi_pl" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_handbrake         = $mem_hambrake" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_7z_autotest       = $mem_7z_autotest" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_7z_compression    = $mem_7z_compression" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_lrzip_compression = $mem_lrzip_compression" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_gzip_compression  = $mem_gzip_compression" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_pigz_compression  = $mem_pigz_compression" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_bzip2_compression = $mem_bzip2_compression" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_lbzip2_compression= $mem_lbzip2_compression" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_pyrit             = $mem_pyrit" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"
echo "mem_hardinfo          = $mem_hardinfo" >> "${BENCHDIR}/repo/${LOGDATE}_rawinfo_mem.log"

mem_sysbench_STAT=`echo $mem_sysbench |awk {'print $32'}`
mem_sysbench_TOTUSED=`echo $mem_sysbench |awk {'print $30'}`
mem_sysbench_SWAPUSED=`echo $mem_sysbench |awk {'print $28'}`

mem_md5_single_py_STAT=`echo $mem_md5_single_py |awk {'print $32'}`
mem_md5_single_py_TOTUSED=`echo $mem_md5_single_py |awk {'print $30'}`
mem_md5_single_py_SWAPUSED=`echo $mem_md5_single_py |awk {'print $28'}`

mem_md5_multi_py_STAT=`echo $mem_md5_multi_py |awk {'print $32'}`
mem_md5_multi_py_TOTUSED=`echo $mem_md5_multi_py |awk {'print $30'}`
mem_md5_multi_py_SWAPUSED=`echo $mem_md5_multi_py |awk {'print $28'}`

mem_md5_single_pl_STAT=`echo $mem_md5_single_pl |awk {'print $32'}`
mem_md5_single_pl_TOTUSED=`echo $mem_md5_single_pl |awk {'print $30'}`
mem_md5_single_pl_SWAPUSED=`echo $mem_md5_single_pl |awk {'print $28'}`

mem_md5_multi_pl_STAT=`echo $mem_md5_multi_pl |awk {'print $32'}`
mem_md5_multi_pl_TOTUSED=`echo $mem_md5_multi_pl |awk {'print $30'}`
mem_md5_multi_pl_SWAPUSED=`echo $mem_md5_multi_pl |awk {'print $28'}`

mem_handbrake_STAT=`echo $mem_handbrake |awk {'print $32'}`
mem_handbrake_TOTUSED=`echo $mem_handbrake |awk {'print $30'}`
mem_handbrake_SWAPUSED=`echo $mem_handbrake |awk {'print $28'}`

mem_7z_autotest_STAT=`echo $mem_7z_autotest |awk {'print $32'}`
mem_7z_autotest_TOTUSED=`echo $mem_7z_autotest |awk {'print $30'}`
mem_7z_autotest_SWAPUSED=`echo $mem_7z_autotest |awk {'print $28'}`

mem_7z_compression_STAT=`echo $mem_7z_compression |awk {'print $32'}`
mem_7z_compression_TOTUSED=`echo $mem_7z_compression |awk {'print $30'}`
mem_7z_compression_SWAPUSED=`echo $mem_7z_compression |awk {'print $28'}`

mem_lrzip_compression_STAT=`echo $mem_lrzip_compression |awk {'print $32'}`
mem_lrzip_compression_TOTUSED=`echo $mem_lrzip_compression |awk {'print $30'}`
mem_lrzip_compression_SWAPUSED=`echo $mem_lrzip_compression |awk {'print $28'}`

mem_gzip_compression_STAT=`echo $mem_gzip_compression |awk {'print $32'}`
mem_gzip_compression_TOTUSED=`echo $mem_gzip_compression |awk {'print $30'}`
mem_gzip_compression_SWAPUSED=`echo $mem_gzip_compression |awk {'print $28'}`

mem_pigz_compression_STAT=`echo $mem_pigz_compression |awk {'print $32'}`
mem_pigz_compression_TOTUSED=`echo $mem_pigz_compression |awk {'print $30'}`
mem_pigz_compression_SWAPUSED=`echo $mem_pigz_compression |awk {'print $28'}`

mem_bzip2_compression_STAT=`echo $mem_bzip2_compression |awk {'print $32'}`
mem_bzip2_compression_TOTUSED=`echo $mem_bzip2_compression |awk {'print $30'}`
mem_bzip2_compression_SWAPUSED=`echo $mem_bzip2_compression |awk {'print $28'}`

mem_lbzip2_compression_STAT=`echo $mem_lbzip2_compression |awk {'print $32'}`
mem_lbzip2_compression_TOTUSED=`echo $mem_lbzip2_compression |awk {'print $30'}`
mem_lbzip2_compression_SWAPUSED=`echo $mem_lbzip2_compression |awk {'print $28'}`

mem_pyrit_STAT=`echo $mem_pyrit |awk {'print $32'}`
mem_pyrit_TOTUSED=`echo $mem_pyrit |awk {'print $30'}`
mem_pyrit_SWAPUSED=`echo $mem_pyrit |awk {'print $28'}`

mem_hardinfo_STAT=`echo $mem_hardinfo |awk {'print $32'}`
mem_hardinfo_TOTUSED=`echo $mem_hardinfo |awk {'print $30'}`
mem_hardinfo_SWAPUSED=`echo $mem_hardinfo |awk {'print $28'}`

mem_GLOBAL_SWAP_WARNING="PASS" 
setglobwarning $mem_sysbench_STAT
setglobwarning $mem_md5_single_py_STAT
setglobwarning $mem_md5_multi_py_STAT
setglobwarning $mem_md5_single_pl_STAT
setglobwarning $mem_md5_multi_pl_STAT
setglobwarning $mem_hambrake_STAT
setglobwarning $mem_7z_autotest_STAT
setglobwarning $mem_7z_compression_STAT
setglobwarning $mem_lrzip_compression_STAT
setglobwarning $mem_gzip_compression_STAT
setglobwarning $mem_pigz_compression_STAT
setglobwarning $mem_bzip2_compression_STAT
setglobwarning $mem_lbzip2_compression_STAT
setglobwarning $mem_pyrit_STAT
setglobwarning $mem_hardinfo_STAT

echo " " >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo " " >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Memoria utilizzata in Kb durante i test" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
if [ "$mem_GLOBAL_SWAP_WARNING" == "WARNING" ]; then
    echo "Attenzione durante i test si è verificata una condizione di swap :: @mem_GLOBAL_SWAP_WARNING = $mem_GLOBAL_SWAP_WARNING"  >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
else
    echo "Tutto OK, nessuna condizione di swap durante i test :: @mem_GLOBAL_SWAP_WARNING = $mem_GLOBAL_SWAP_WARNING"  >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
fi

echo "Test sysbench            :: @mem_sysbench_STAT               = $mem_sysbench_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test sysbench            :: @mem_sysbench_TOTUSED            = $mem_sysbench_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test sysbench            :: @mem_sysbench_SWAPUSED           = $mem_sysbench_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test md5.py singlethread :: @mem_md5_single_py_STAT          = $mem_md5_single_py_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.py singlethread :: @mem_md5_single_py_TOTUSED       = $mem_md5_single_py_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.py singlethread :: @mem_md5_single_py_SWAPUSED      = $mem_md5_single_py_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test md5.py multithread  :: @mem_md5_multi_py_STAT           = $mem_md5_multi_py_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.py multithread  :: @mem_md5_multi_py_TOTUSED        = $mem_md5_multi_py_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.py multithread  :: @mem_md5_multi_py_SWAPUSED       = $mem_md5_multi_py_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test md5.pl singlethread :: @mem_md5_single_pl_STAT          = $mem_md5_single_pl_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.pl singlethread :: @mem_md5_single_pl_TOTUSED       = $mem_md5_single_pl_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.pl singlethread :: @mem_md5_single_pl_SWAPUSED      = $mem_md5_single_pl_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test md5.pl multithread  :: @mem_md5_multi_pl_STAT           = $mem_md5_multi_pl_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.pl multithread  :: @mem_md5_multi_pl_TOTUSED        = $mem_md5_multi_pl_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test md5.pl multithread  :: @mem_md5_multi_pl_SWAPUSED       = $mem_md5_multi_pl_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test handbrake           :: @mem_handbrake_STAT              = $mem_handbrake_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test handbrake           :: @mem_handbrake_TOTUSED           = $mem_handbrake_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test handbrake           :: @mem_handbrake_SWAPUSED          = $mem_handbrake_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test 7z Autotest         :: @mem_7z_autotest_STAT            = $mem_7z_autotest_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test 7z Autotest         :: @mem_7z_autotest_TOTUSED         = $mem_7z_autotest_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test 7z Autotest         :: @mem_7z_autotest_SWAPUSED        = $mem_7z_autotest_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test 7z Compression      :: @mem_7z_compression_STAT         = $mem_7z_compression_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test 7z Compression      :: @mem_7z_compression_TOTUSED      = $mem_7z_compression_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test 7z Compression      :: @mem_7z_compression_SWAPUSED     = $mem_7z_compression_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test lrzip Compression   :: @mem_lrzip_compression_STAT      = $mem_lrzip_compression_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test lrzip Compression   :: @mem_lrzip_compression_TOTUSED   = $mem_lrzip_compression_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test lrzip Compression   :: @mem_lrzip_compression_SWAPUSED  = $mem_lrzip_compression_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test gzip Compression    :: @mem_gzip_compression_STAT       = $mem_gzip_compression_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test gzip Compression    :: @mem_gzip_compression_TOTUSED    = $mem_gzip_compression_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test gzip Compression    :: @mem_gzip_compression_SWAPUSED   = $mem_gzip_compression_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test pigz Compression    :: @mem_pigz_compression_STAT       = $mem_pigz_compression_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test pigz Compression    :: @mem_pigz_compression_TOTUSED    = $mem_pigz_compression_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test pigz Compression    :: @mem_pigz_compression_SWAPUSED   = $mem_pigz_compression_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test bzip2 Compression   :: @mem_bzip2_compression_STAT      = $mem_bzip2_compression_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test bzip2 Compression   :: @mem_bzip2_compression_TOTUSED   = $mem_bzip2_compression_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test bzip2 Compression   :: @mem_bzip2_compression_SWAPUSED  = $mem_bzip2_compression_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test lbzip2 Compression  :: @mem_lbzip2_compression_STAT     = $mem_lbzip2_compression_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test lbzip2 Compression  :: @mem_lbzip2_compression_TOTUSED  = $mem_lbzip2_compression_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test lbzip2 Compression  :: @mem_lbzip2_compression_SWAPUSED = $mem_lbzip2_compression_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test Pyrit               :: @mem_pyrit_STAT                  = $mem_pyrit_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test Pyrit               :: @mem_pyrit_TOTUSED               = $mem_pyrit_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test Pyrit               :: @mem_pyrit_SWAPUSED              = $mem_pyrit_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"

echo "Test Hardinfo            :: @mem_hardinfo_STAT               = $mem_hardinfo_STAT" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test Hardinfo            :: @mem_hardinfo_TOTUSED            = $mem_hardinfo_TOTUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"
echo "Test Hardinfo            :: @mem_hardinfo_SWAPUSED           = $mem_hardinfo_SWAPUSED" >> "${BENCHDIR}/repo/${LOGDATE}_freemem.log"




}
 
