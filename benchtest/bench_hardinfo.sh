
function prepare_hardinfo_value {
    if [ $HARDINFO_TEST_RIUSCITO -eq 0 ]; then
        #effettua un giro di test solo se non è già riuscito
        if [ $HARDINFO_test -ne 0 ]; then 
            #e se il flag del test è impostato per effettuare il test
            svuotamemoria "hardinfo"
            #NOTA fatto cosi perchè altrimenti -r >qualcosa mi catturava l'output VUOTO di xterm, anzichè di hardinfo
            cmd='xterm -l -lf "'${BENCHDIR}/tmp/xterm.hardinfo.log'" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e '"'"'hardinfo -r | tee "'${BENCHDIR}/repo/${LOGDATE}_RawOut_hardinfo.log'"'"'"
            HARDINFOFILESIZE=0
            let maxcounter=1
            while [ $HARDINFOFILESIZE -eq 0 ]; do
                (( maxcounter++ ))
                safe_delete_file "${BENCHDIR}/tmp/xterm.hardinfo.log"
                if [ $maxcounter -gt 3 ]; then
                    echo "Tutti i 3 tentativi di gererare il report di hardinfo sono falliti"
                    break
                else
                    eval "$cmd"
                    sleep 0.1
                    HARDINFOFILESIZE=$(stat -c%s "${BENCHDIR}/repo/${LOGDATE}_RawOut_hardinfo.log")
                    if [ $HARDINFOFILESIZE -eq 0 ]; then
                        echo "hardinfo è fallito riprovo ancora - Tentativo  $maxcounter di 3"
                    else
                        HARDINFO_TEST_RIUSCITO=1
                    fi
                fi
            done
        fi
    fi
}


function write_hardinfo_test_value {
    if [ $HARDINFO_test -ne 0 ]; then
    
        if [ $HARDINFOFILESIZE -eq 0 ]; then
            echo "HARDINFO TEST NON DISPONIBILE PER QUESTA MACCHINA                                      :: @hinf_status     = FAIL" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        else
            FLAG_Blowfish=0
            FLAG_CryptoHash=0
            FLAG_Fibonacci=0
            FLAG_NQueens=0
            FLAG_FFT=0
            FLAG_Raytracing=0

            rgxp_SET_Blowfish="^\-CPU\sBlowfish\-$"
            rgxp_SET_CryptoHash="^\-CPU\sCryptoHash\-$"
            rgxp_SET_Fibonacci="^\-CPU\sFibonacci\-$"
            rgxp_SET_NQueens="^\-CPU\sN\-Queens\-$"
            rgxp_SET_FFT="^\-FPU\sFFT\-$"
            rgxp_SET_Raytracing="^\-FPU\sRaytracing\-$"
            rgex_GETVAL="^<big><b>This\sMachine</b></big>\s[[:digit:]]*\s*MHz\s*(.*)\s*$"

            while read -r line; do
                #echo "PIPPO"$line
                #Togliere i vari echo di DEBUG per avere un output pulito
                # "-CPU Blowfish-"
                # "-CPU CryptoHash-"
                # "-CPU Fibonacci-"
                # "-CPU N-Queens-"
                # "-FPU FFT-"
                # "-FPU Raytracing-"
                
                if [ $FLAG_Blowfish -ne 0 ]; then
                    if [[ "$line" =~ $rgex_GETVAL ]] ; then
                        FLAG_Blowfish=0
                        VAL_Blowfish="${BASH_REMATCH[1]}"
                        #echo Blowfish=$VAL_Blowfish
                    fi
                elif [ $FLAG_CryptoHash -ne 0 ]; then
                    if [[ "$line" =~ $rgex_GETVAL ]] ; then
                        FLAG_CryptoHash=0
                        VAL_CryptoHash="${BASH_REMATCH[1]}"
                        #echo CryptoHash=$VAL_CryptoHash
                    fi
                elif [ $FLAG_Fibonacci -ne 0 ]; then
                    if [[ "$line" =~ $rgex_GETVAL ]] ; then
                        FLAG_Fibonacci=0
                        VAL_Fibonacci="${BASH_REMATCH[1]}"
                        #echo Fibonacci=$VAL_Fibonacci
                    fi
                elif [ $FLAG_NQueens -ne 0 ]; then
                    if [[ "$line" =~ $rgex_GETVAL ]] ; then
                        FLAG_NQueens=0
                        VAL_NQueens="${BASH_REMATCH[1]}"
                        #echo NQueens=$VAL_NQueens
                    fi    
                elif [ $FLAG_FFT -ne 0 ]; then
                    if [[ "$line" =~ $rgex_GETVAL ]] ; then
                        FLAG_FFT=0
                        VAL_FFT="${BASH_REMATCH[1]}"
                        #echo FFT=$VAL_FFT
                    fi
                elif [ $FLAG_Raytracing -ne 0 ]; then
                    if [[ "$line" =~ $rgex_GETVAL ]] ; then
                        FLAG_Raytracing=0
                        VAL_Raytracing="${BASH_REMATCH[1]}"
                        #echo Raytracing=$VAL_Raytracing
                    fi
                else
                    if [[ "$line" =~ $rgxp_SET_Blowfish ]] ; then
                        FLAG_Blowfish=1
                        #echo "TROVATO Blowfish" $FLAG_Blowfish
                    elif [[ "$line" =~ $rgxp_SET_CryptoHash ]] ; then
                        FLAG_CryptoHash=1
                        #echo "TROVATO"
                    elif [[ "$line" =~ $rgxp_SET_Fibonacci ]] ; then
                        FLAG_Fibonacci=1
                        #echo "TROVATO"
                    elif [[ "$line" =~ $rgxp_SET_NQueens ]] ; then
                        FLAG_NQueens=1
                        #echo "TROVATO"
                    elif [[ "$line" =~ $rgxp_SET_FFT ]] ; then
                        FLAG_FFT=1
                        #echo "TROVATO"
                    elif [[ "$line" =~ $rgxp_SET_Raytracing ]] ; then
                        FLAG_Raytracing=1
                        #echo "TROVATO"
                    fi
                fi
            done < "${BENCHDIR}/repo/${LOGDATE}_RawOut_hardinfo.log"
#done < <(hardinfo -r) #NON USARE QUESTA FORMA. di solito funziona, ma per questo comando qualche volta (non sempre) da un segtault            
            echo "HARDINFO TEST                                                                          :: @hinf_status     = PASS" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            echo "Blowfish                                                              (basso è meglio) :: @hinf_Blowfish   = $VAL_Blowfish" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            echo "CryptoHash                                                            (alto  è meglio) :: @hinf_CryptoHash = $VAL_CryptoHash" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            echo "Fibonacci                                                             (basso è meglio) :: @hinf_Fibonacci  = $VAL_Fibonacci" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            echo "NQueens                                                               (basso è meglio) :: @hinf_NQueens    = $VAL_NQueens" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            echo "FFT                                                                   (basso è meglio) :: @hinf_FFT        = $VAL_FFT" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            echo "Raytracing                                                            (basso è meglio) :: @hinf_Raytracing = $VAL_Raytracing" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
        fi
    else
        echo "HARDINFO TEST NON SELEZIONATO                                                          :: @hinf_status     = SKIP" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
    fi
}
 
