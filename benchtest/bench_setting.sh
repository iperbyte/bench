# DA COMPLETARE CON LETTURA DA .conf

ONLYHARDINFO=0 #NESSUN TEST,Estrae solo le info sull'hardware (PER ORA NON GESTITO)
FAKEBENCH=0 #0 bench Veri, 1 bench falsi e rapidi solo per testare l'output 
#Variabili per l'esecuzione del test md5
BENCHMD5=1 #0=NO 1=SI
    BENCHMD5_PY=1 #0=NO 1=SI
        BENCHMD5_PY_LIN=10  #0=NO num=iterazioni
        BENCHMD5_PY_PAR=100 #0=NO num=iterazioni
    BENCHMD5_PL=1 #0=NO 1=SI
        BENCHMD5_PL_LIN=10  #0=NO num=iterazioni
        BENCHMD5_PL_PAR=100 #0=NO num=iterazioni

# Handbrake Test
HANDB_test=1
    HANDB_Test_x264_mp4=1
    #HANDB_Test_x264_mkv=1 Eliminato l'algoritmo di compressione è praticamente lo stesso. Cambia il muxing, e l'altro mi sembra lavori un pelino meglio in multitread
# Handbrake Profile
HANDB_x264_mp4='-e x264 --x264-preset veryslow --x264-tune film -q 22.0 -X 1280 -x b-adapt=4:rc-lookahead=50 -E faac -B 192 -4 -O --start-at duration:60 --stop-at duration:120'  
HANDB_x264_mkv='-e x264 --x264-preset veryslow --x264-tune film -q 22.0 -X 1280 -x b-adapt=4:rc-lookahead=50 -E lame -B 192 --start-at duration:60 --stop-at duration:120'

P7ZIP_test=1
    P7ZIP_bench=1
    P7ZIP_Standard=1
    LRZIP_test=1
    GZIP_test=1
    PIGZ_test=1
    BZIP2_test=1
    LBZIP2_test=1
    

    
SETTEZIP_PRESET="7z a -t7z '${BENCHDIR}/tmp/ToS-4k-1920_z9.7z' '${BENCHDIR}/data/ToS-4k-1920.mov' -mx5 -mmt=on -ms=on -m0=lzma2"
SETLRZIP_PRESET="lrzip -L 9 -N -20 -T -f -L 9 -o '${BENCHDIR}/tmp/ToS-4k-1920.lrz' '${BENCHDIR}/data/ToS-4k-1920.mov'"
echo "gzip -9 -c '${BENCHDIR}/data/ToS-4k-1920.mov' >'${BENCHDIR}/tmp/ToS-4k-1920_gzip.gz'" >"${BENCHDIR}/tmp/bench_gzip.sh"
echo "pigz -9 -k -c '${BENCHDIR}/data/ToS-4k-1920.mov' >'${BENCHDIR}/tmp/ToS-4k-1920_pigz.gz'" >"${BENCHDIR}/tmp/bench_pigz.sh"
echo "bzip2 -9 -c '${BENCHDIR}/data/ToS-4k-1920.mov' >'${BENCHDIR}/tmp/ToS-4k-1920_bzip2.bz2'" >"${BENCHDIR}/tmp/bench_bzip2.sh"
echo "lbzip2 -9 -c '${BENCHDIR}/data/ToS-4k-1920.mov' >'${BENCHDIR}/tmp/ToS-4k-1920_lbzip2.bz2'" >"${BENCHDIR}/tmp/bench_lbzip2.sh"

SYSBENCH=1
echo "time sysbench --num-threads=$INFO_TOT_THREAD --test=cpu --cpu-max-prime=50000 run |tee '${BENCHDIR}/repo/${LOGDATE}_RawOut_sysbench.log'" >"${BENCHDIR}/tmp/bench_sysbench.sh"

PYRIT_test=1
SETPYRIT_TEST='pyrit benchmark_long'

HARDINFO_test=1

# DECOMMENTA per avere i test in quasi 0 (solo per verificare output)

if [ $FAKEBENCH -ne 0 ]; then
 HANDB_x264_mp4='-e x264 --x264-preset veryslow --x264-tune film -q 22.0 -X 1280 -x b-adapt=4:rc-lookahead=50 -E faac -B 192 -4 -O --start-at duration:60 --stop-at duration:1'  
 HANDB_x264_mkv='-e x264 --x264-preset veryslow --x264-tune film -q 22.0 -X 1280 -x b-adapt=4:rc-lookahead=50 -E lame -B 192 --start-at duration:60 --stop-at duration:1'
 BENCHMD5_PY_LIN=1  #0=NO num=iterazioni
 BENCHMD5_PY_PAR=1  #0=NO num=iterazioni
 BENCHMD5_PL_LIN=1  #0=NO num=iterazioni
 BENCHMD5_PL_PAR=1  #0=NO num=iterazioni
 SETPYRIT_TEST='pyrit benchmark'
 SETTEZIP_PRESET="7z a -t7z '${BENCHDIR}/tmp/ToS-4k-1920_z9.faketest.7z' '${BENCHDIR}/tmp/ToS-4k-1920.mov.faketest' -mx5 -mmt=on -ms=on -m0=lzma2"
 SETLRZIP_PRESET="lrzip -L 9 -N -20 -T -f -L 9 -o '${BENCHDIR}/tmp/ToS-4k-1920.faketest.lrz' '${BENCHDIR}/tmp/ToS-4k-1920.mov.faketest'"
 echo "gzip -9 -c '${BENCHDIR}/tmp/ToS-4k-1920.mov.faketest' >'${BENCHDIR}/tmp/ToS-4k-1920_gzip.faketest.gz'" >"${BENCHDIR}/tmp/bench_gzip.sh"
 echo "pigz -9 -k -c '${BENCHDIR}/tmp/ToS-4k-1920.mov.faketest' >'${BENCHDIR}/tmp/ToS-4k-1920_pigz.faketest.gz'" >"${BENCHDIR}/tmp/bench_pigz.sh"
 echo "bzip2 -9 -c '${BENCHDIR}/tmp/ToS-4k-1920.mov.faketest' >'${BENCHDIR}/tmp/ToS-4k-1920_bzip2.faketest.bz2'" >"${BENCHDIR}/tmp/bench_bzip2.sh"
 echo "lbzip2 -9 -c '${BENCHDIR}/tmp/ToS-4k-1920.mov.faketest' >'${BENCHDIR}/tmp/ToS-4k-1920_lbzip2.faketest.bz2'" >"${BENCHDIR}/tmp/bench_lbzip2.sh"
 echo "time sysbench --num-threads=$INFO_TOT_THREAD --test=cpu --cpu-max-prime=50 run |tee '${BENCHDIR}/repo/${LOGDATE}_RawOut_sysbench.log'" >"${BENCHDIR}/tmp/bench_sysbench.sh"
fi
echo "nice -n -20 HandBrakeCLI -i "$BENCHDIR/data/ToS-4k-1920.mov $HANDB_x264_mp4 -o "${BENCHDIR}/tmp/HANDB_x264.mp4 " >"${BENCHDIR}/tmp/handb.sh"
echo "------------------- ${SCRIPTDIR}"
echo 'for i in `seq '$BENCHMD5_PY_LIN'` ; do "'${SCRIPTDIR}/benchmd5.py'" $i; done ' >"${BENCHDIR}/tmp/benchmd5_py_mono.sh"
echo 'for i in `seq '$BENCHMD5_PY_LIN'` ; do "'${SCRIPTDIR}/benchmd5.pl'" $i; done ' >"${BENCHDIR}/tmp/benchmd5_pl_mono.sh"
echo "seq $BENCHMD5_PY_PAR |parallel --gnu '$SCRIPTDIR/benchmd5.py'" >"${BENCHDIR}/tmp/benchmd5_py_multi.sh"
echo "seq $BENCHMD5_PY_PAR |parallel --gnu '$SCRIPTDIR/benchmd5.pl'" >"${BENCHDIR}/tmp/benchmd5_pl_multi.sh"

chmod +x "${BENCHDIR}/tmp/benchmd5_py_mono.sh"
chmod +x "${BENCHDIR}/tmp/benchmd5_pl_mono.sh"
chmod +x "${BENCHDIR}/tmp/benchmd5_py_multi.sh"
chmod +x "${BENCHDIR}/tmp/benchmd5_pl_multi.sh"
chmod +x "${BENCHDIR}/tmp/bench_gzip.sh"
chmod +x "${BENCHDIR}/tmp/bench_pigz.sh"
chmod +x "${BENCHDIR}/tmp/bench_bzip2.sh"
chmod +x "${BENCHDIR}/tmp/bench_lbzip2.sh"
chmod +x "${BENCHDIR}/tmp/bench_sysbench.sh"
chmod +x "${BENCHDIR}/tmp/handb.sh"


echo fake > "${BENCHDIR}/tmp/ToS-4k-1920.mov.faketest"

 
