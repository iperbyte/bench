function turbostatminmax {
gawk -v cols="GHz;TSC" '
   BEGIN {
        min1=9999999.99
        min2=9999999.99
        max1=0
        max2=0
        n=split(cols,ar,";")
        for ( i = 1; i <= n; i++ ) {
           #print i,ar[i]
        }
    }
    ( /^cor\s+CPU.*/ ) {
        k=NR
        trovato=1
        p=1
        for ( i = 1; i <= NF; i++ ) {
            #print "HH ",$i
            for ( j = 1; j <= n; j++ ){
                sub( /\r/,"",$i) #Aggiunto perche nelloutput ce un \r
                #print "::"$i"**"
                #print "-------------- i="i,"j="j,colName,$i"<>",ar[j]
                #print "REGEXP=-"identifier_regexp"-"
                if ( ar[j] == $i ){
                    s[j]=i
                    #print "pos=",j,s[j]
                }
            }
        }
    }
    ( (NR>k+1) && (trovato==1) ) {
        sub( /\r/,"",$s[2]) #Aggiunto perche nelloutput ce un \r (su $[1] non serve)
        #sub( /\r/,"",$0) #Aggiunto perche nelloutput ce un \r Serve soloper print successivo
        #print s[1],s[2],"---",$s[1],$s[2],":",$0
        if ( $s[1] < min1 ) { min1 = $s[1]}
        if ( $s[2] < min2 ) { min2 = $s[2]}
        if ( $s[1] > max1 ) { max1 = $s[1]}
        if ( $s[1] > max2 ) { max2 = $s[1]}
    }
    END { print "minGHz",min1,"minTSC",min2,"maxGHz",max1,"maxTSC",max2 }
' "$1"
}

function leggiturbobost {
#Test Proc intel
echo "Verifica se turbostat è disponibile per il processore - Test: No invariant TSC e Test No APERF!"
xterm -l -lf "${BENCHDIR}/tmp/xterm.turbostat_check.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e turbostat -v -i 1 wait 0.1
grep "No invariant TSC" "${BENCHDIR}/tmp/xterm.turbostat_check.log"
TurboStatCheck=$? 
#Test Proc Amd
grep "No APERF!" "${BENCHDIR}/tmp/xterm.turbostat_check.log"
TurboStatCheck1=$? 

#Se $TurboStatCheck è 0 allora dentro al file ${BENCHDIR}/tmp/xterm.turbostat_check.log c'è la scritta "No invariant TSC"
#quindi Turbostat non funziona

prepare_hardinfo_value
sleep 2
if [ $TurboStatCheck -eq 0 ]; then
    echo "Turbostat General info [[[ @turbostatmemo" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    cat "${BENCHDIR}/tmp/xterm.turbostat_check.log"|grep -e "CPUID" -e "*" -e "No invariant TSC" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "TURBO STAT NON DISPONIBILE SU QUESTA CPU" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "]]]" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
elif [ $TurboStatCheck1 -eq 0 ]; then
    echo "Turbostat General info [[[ @turbostatmemo" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    cat "${BENCHDIR}/tmp/xterm.turbostat_check.log"|grep -e "CPUID" -e "*" -e "No APERF!" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "TURBO STAT NON DISPONIBILE SU QUESTA CPU" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "]]]" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
else
    echo "test turboboost a riposo per 20 secondi"
    xterm -l -lf "${BENCHDIR}/tmp/xterm.turbostat_riposo.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e turbostat -v -i 1 &
    procpid=$!
    if [ $FAKEBENCH -eq 0 ]; then 
        sleep 21
    else
        sleep 2
    fi
    disown $procpid
    kill -9 $procpid > /dev/null 2>&1
    minmax=$(turbostatminmax "${BENCHDIR}/tmp/xterm.turbostat_riposo.log")
    echo "" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "**************************** TURBOSTAT INFO ********************************" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "Turbostat General info [[[ @turbostatmemo" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    cat "${BENCHDIR}/tmp/xterm.turbostat_riposo.log"|grep -e "CPUID" -e "*" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "]]]" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    riposomin=`echo $minmax |awk {'print $2'}`
    riposomintsc=`echo $minmax |awk {'print $4'}`
    echo "Turbostat Valore minimo a riposo GHz                    :: @riposomin         = $riposomin">>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "Turbostat Valore minimo a riposo TSC                    :: @riposomintsc      = $riposomintsc" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    
    prepare_hardinfo_value
    sleep 2
    echo "test turboboost su un thread per 20 secondi"
    xterm -l -lf "${BENCHDIR}/tmp/xterm.turbostat_1Core.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e turbostat -v -i 1 &
    procpid=$!
    if [ $FAKEBENCH -eq 0 ]; then 
        stress -c 1 -t 21 > /dev/null 2>&1
    else
        stress -c 1 -t 2 > /dev/null 2>&1
    fi
    
    disown $procpid
    kill -9 $procpid > /dev/null 2>&1
    minmax=$(turbostatminmax "${BENCHDIR}/tmp/xterm.turbostat_1Core.log")
    stress1coremax=`echo $minmax |awk {'print $6'}`
    stress1coremaxtsc=`echo $minmax |awk {'print $8'}`
    #echo $stress1coremax
    #echo $stress1coremaxtsc
    echo "Turbostat Valore massimo con 1 thread al lavoro GHz     :: @stress1coremax    = $stress1coremax">>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "Turbostat Valore massimo con 1 thread al lavoro TSC     :: @stress1coremaxtsc = $stress1coremaxtsc" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    
    prepare_hardinfo_value
    sleep 2
    echo "test turboboost su tutti i thread per 20 secondi"
    xterm -l -lf "${BENCHDIR}/tmp/xterm.turbostat_Full_Core.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e turbostat -v -i 1 &
    procpid=$!
        if [ $FAKEBENCH -eq 0 ]; then 
        stress -c $INFO_TOT_THREAD -t 21 > /dev/null 2>&1
    else
        stress -c $INFO_TOT_THREAD -t 2 > /dev/null 2>&1
    fi
    disown $procpid
    kill -9 $procpid > /dev/null 2>&1
    minmax=$(turbostatminmax "${BENCHDIR}/tmp/xterm.turbostat_Full_Core.log")
    stressfullcoremax=`echo $minmax |awk {'print $6'}`
    stressfullcoremaxtsc=`echo $minmax |awk {'print $8'}`
    #cho $stressfullcoremax
    #echo $stressfullcoremaxtsc
    echo "Turbostat Valore massimo con tutti thread al lavoro GHz :: @stressfullcoremax = $stressfullcoremax">>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
    echo "Turbostat Valore massimo con tutti thread al lavoro TSC :: @stressfullcoremax = $stressfullcoremaxtsc" >>"${BENCHDIR}/repo/${LOGDATE}_turbostat.log"
fi
}
 
