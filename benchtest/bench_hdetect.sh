let i=1
declare -a array_x86info
#echo A
while read -r line; do
    echo "$line" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
    #echo "$line"
    # Devo definirle prima in variabili perche direttamente fa cose strane
    rgxp_SOKET="^Socket\sDesignation:\s(.*)$"
    rgxp_ID="^ID:\s(.*)$"
    rgxp_EFAMILY="^Family:\s+(.*)$"
    rgxp_FAMILY="^Signature:\s+Type\s+(\w+)\s*,\s+Family\s+(\w+)\s*,\s+Model\s+(\w+)\s*,\s+Stepping\s+(\w+)\s*.*"
    rgxp_NAME="^Version:\s(.*)$"
    rgxp_VOLTAGE="^Voltage:\s*(\w*)\s*\s*V$"
    rgxp_ECLOCK="^External\sClock:\s(.*)$"
    rgxp_MAXSPEED="^Max\sSpeed:\s(.*)$"
    rgxp_CORE="^Core\sCount:\s(.*)$"
    rgxp_THREADxCORE="^Thread\sCount:\s(.*)$"
    if [[ "$line" =~ $rgxp_SOKET ]] ; then 
        INFO_SOKET_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_SOKET_DMIDECODE
    elif [[ "$line" =~ $rgxp_ID ]] ; then 
        INFO_ID_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_ID_DMIDECODE
    elif [[ "$line" =~ $rgxp_EFAMILY ]] ; then 
        INFO_EFAMILY_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_EFAMILY_DMIDECODE
    elif [[ "$line" =~ $rgxp_FAMILY ]] ; then 
        INFO_TYPE_DMIDECODE="${BASH_REMATCH[1]}" 
        INFO_FAMILY_DMIDECODE="${BASH_REMATCH[2]}"
        INFO_MODEL_DMIDECODE="${BASH_REMATCH[3]}"
        INFO_STEPPING_DMIDECODE="${BASH_REMATCH[4]}"
        #echo INFO_TYPE_DMIDECODE $INFO_TYPE_DMIDECODE "NON SO COSA SIA POTREBBE CORRISPONDERE A TYPE DI x86info"
        #echo INFO_FAMILY_DMIDECODE $INFO_FAMILY_DMIDECODE
        #echo INFO_MODEL_DMIDECODE $INFO_MODEL_DMIDECODE
        #echo INFO_STEPPING_DMIDECODE $INFO_STEPPING_DMIDECODE
        
    elif [[ "$line" =~ $rgxp_NAME ]] ; then 
        INFO_NAME_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_NAME_DMIDECODE
    elif [[ "$line" =~ $rgxp_VOLTAGE ]] ; then 
        INFO_VOLTAGE_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_VOLTAGE_DMIDECODE
    elif [[ "$line" =~ $rgxp_ECLOCK ]] ; then 
        INFO_ECLOCK_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_ECLOCK_DMIDECODE
    elif [[ "$line" =~ $rgxp_MAXSPEED ]] ; then 
        INFO_MAXSPEED_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_MAXSPEED_DMIDECODE
    elif [[ "$line" =~ $rgxp_CORE ]] ; then 
        INFO_CORE_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_CORE_DMIDECODE
    elif [[ "$line" =~ $rgxp_THREADxCORE ]] ; then 
        INFO_THREADxCORE_DMIDECODE="${BASH_REMATCH[1]}"
        #echo ">>>>>>>>" $INFO_THREADxCORE_DMIDECODE
    fi
    #qua dentro ci sono anche i flag interessanti, ma da x86info sembrano piu comodi
done < <(dmidecode --type processor)

echo "- - LETTE DA DMIDECODE - -" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "SOKET       >" $INFO_SOKET_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "ID          >" $INFO_ID_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "EFAMILY     >" $INFO_EFAMILY_DMIDECODE "Non so cosa sia potrebbe corrispondere ad Extended family di x86info" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "TYPE        >" $INFO_TYPE_DMIDECODE "Non so cosa sia potrebbe corrispondere ad Extended type di x86info" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "FAMILY      >" $INFO_FAMILY_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "MODEL       >" $INFO_MODEL_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "STEPPING    >" $INFO_STEPPING_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "NAME        >" $INFO_NAME_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "VOLTAGE     >" $INFO_VOLTAGE_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "ECLOCK      >" $INFO_ECLOCK_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "MAXSPEED    >" $INFO_MAXSPEED_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "CORE        >" $INFO_CORE_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"
echo "THREADxCORE >" $INFO_THREADxCORE_DMIDECODE >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_dmidecode_type_processor.log"

#echo B
FLAG1=0
FLAG2=0    
while read -r line; do
    echo "$line" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
    # Devo definirle prima in variabili perche direttamente fa cose strane
    
    rgxp_FAMILY="^Extended\sFamily:\s+(\w+)\s+Extended\sModel:\s+(\w+)\s+Family:\s+(\w+)\s+Model:\s+(\w+)\s+Stepping:\s+(\w+)\s*$"
    rgxp_FAMILYOLD="^EFamily:\s+(\w+)\s+EModel:\s+(\w+)\s+Family:\s+(\w+)\s+Model:\s+(\w+)\s+Stepping:\s+(\w+)\s*$"
    rgxp_NAME="^Processor\sname string.*:\s(.*)$" 
    rgxp_Flag1="^Feature\sflags:\s*$"
    rgxp_Flag2="^Extended\sfeature\sflags:\s*$"
    rgxp_LONG_NOPS="^Long\sNOPs\ssupported:\s(.*)$"
    rgxp_THREAD="^Total\sprocessor\sthreads:\s(.*)$"
    rgxp_SYS="^This\ssystem\shas\s(.*)\srunning\s.*"
    if [ $FLAG1 -ne 0 ]; then
        INFO_FLAG_X86INFO=$line
        FLAG1=0
    elif [ $FLAG2 -ne 0 ]; then
        INFO_EXFLAG_NOPS_X86INFO=$line
        FLAG2=0
    else
        if [[ "$line" =~ $rgxp_FAMILY ]] ; then 
            INFO_EFAMILY_X86INFO="${BASH_REMATCH[1]}"
            INFO_EMODEL_X86INFO="${BASH_REMATCH[2]}"
            INFO_FAMILY_X86INFO="${BASH_REMATCH[3]}"
            INFO_MODEL_X86INFO="${BASH_REMATCH[4]}"
            INFO_STEPPING_X86INFO="${BASH_REMATCH[5]}"

            #echo ">>>>>>>EFAMILY >" $INFO_EFAMILY_X86INFO
            #echo ">>>>>>>EMODEL  >" $INFO_EMODEL_X86INFO
            #echo ">>>>>>>FAMILY  >" $INFO_FAMILY_X86INFO
            #echo ">>>>>>>MODEL   >" $INFO_MODEL_X86INFO
            #echo ">>>>>>>STEPPING>" $INFO_STEPPING_X86INFO
        elif [[ "$line" =~ $rgxp_FAMILYOLD ]] ; then 
            INFO_EFAMILY_X86INFO="${BASH_REMATCH[1]}"
            INFO_EMODEL_X86INFO="${BASH_REMATCH[2]}"
            INFO_FAMILY_X86INFO="${BASH_REMATCH[3]}"
            INFO_MODEL_X86INFO="${BASH_REMATCH[4]}"
            INFO_STEPPING_X86INFO="${BASH_REMATCH[5]}"

            #echo ">>>>>>>EFAMILY >" $INFO_EFAMILY_X86INFO
            #echo ">>>>>>>EMODEL  >" $INFO_EMODEL_X86INFO
            #echo ">>>>>>>FAMILY  >" $INFO_FAMILY_X86INFO
            #echo ">>>>>>>MODEL   >" $INFO_MODEL_X86INFO
            #echo ">>>>>>>STEPPING>" $INFO_STEPPING_X86INFO
        elif [[ "$line" =~ $rgxp_NAME ]] ; then 
            INFO_NAME_X86INFO="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_NAME_X86INFO
        elif [[ "$line" =~ $rgxp_Flag1 ]] ; then 
            #echo ">>>>>>>> SET Flag1"
            FLAG1=1
        elif [[ "$line" =~ $rgxp_Flag2 ]] ; then 
            #echo ">>>>>>>> SET Flag2"
            FLAG2=2
        elif [[ "$line" =~ $rgxp_LONG_NOPS ]] ; then 
            INFO_LONG_NOPS_X86INFO="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_LONG_NOPS_X86INFO
        elif [[ "$line" =~ $rgxp_THREAD ]] ; then 
            INFO_THREAD_X86INFO="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_THREAD_X86INFO
        elif [[ "$line" =~ $rgxp_SYS ]] ; then 
            INFO_SYS_X86INFO="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_SYS_X86INFO
        fi
    fi
done < <(x86info -f)

echo "- - LETTE DA X86INFO - -" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "EFAMILY    >" $INFO_EFAMILY_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "EMODEL     >" $INFO_EMODEL_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "FAMILY     >" $INFO_FAMILY_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "MODEL      >" $INFO_MODEL_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "STEPPING   >" $INFO_STEPPING_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "NAME       >" $INFO_NAME_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "LONGNOPS   >" $INFO_LONG_NOPS_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "THREAD     >" $INFO_THREAD_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "SYS        >" $INFO_SYS_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "FLAG       >" $INFO_FLAG_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
echo "EXFLAGNOPS >" $INFO_EXFLAG_NOPS_X86INFO >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_x86info_f.log"
#echo C
while read -r line; do
    echo "$line" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
    # Devo definirle prima in variabili perche direttamente fa cose strane
    rgxp_VIRTUALIZATION="^Virtualization:\s(.*)$"
    rgxp_CPUMHZ="^CPU MHz:\s(.*)$"
    rgxp_L1DCACHE="^L1d cache:\s(.*)$"
    rgxp_L1ICACHE="^L1i cache:\s(.*)$"
    rgxp_L2CACHE="^L2 cache:\s(.*)$"
    rgxp_L3CACHE="^L3 cache:\s(.*)$"
        if [[ "$line" =~ $rgxp_VIRTUALIZATION ]] ; then 
            INFO_VIRTUALIZATION_LSCPU="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_VIRTUALIZATION_LSCPU
        elif [[ "$line" =~ $rgxp_CPUMHZ ]] ; then 
            INFO_CPUMHZ_LSCPU="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_CPUMHZ_LSCPU
        elif [[ "$line" =~ $rgxp_L1DCACHE ]] ; then 
            INFO_L1DCACHE_LSCPU="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_L1DCACHE_LSCPU
        elif [[ "$line" =~ $rgxp_L1ICACHE ]] ; then 
            INFO_L1ICACHE_LSCPU="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_L1ICACHE_LSCPU
        elif [[ "$line" =~ $rgxp_L2CACHE ]] ; then 
            INFO_L2CACHE_LSCPU="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_L2CACHE_LSCPU
        elif [[ "$line" =~ $rgxp_L3CACHE ]] ; then 
            INFO_L3CACHE_LSCPU="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_L3CACHE_LSCPU
        fi
done < <(lscpu)

echo "- - LETTE DA LSCPU - -" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
echo "VIRTUALIZATION >" $INFO_VIRTUALIZATION_LSCPU >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
echo "CPUMHZ         >" $INFO_CPUMHZ_LSCPU >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
echo "L1DCACHE       >" $INFO_L1DCACHE_LSCPU >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
echo "L1ICACHE       >" $INFO_L1ICACHE_LSCPU >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
echo "L2CACHE        >" $INFO_L2CACHE_LSCPU >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
echo "L3CACHE        >" $INFO_L3CACHE_LSCPU >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_lscpu.log"
#echo D
while read -r line; do
    echo "$line" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
    # Devo definirle prima in variabili perche direttamente fa cose strane
    rgxp_PROCNUM="^processor\s*:\s(.*)$"
    rgxp_NAME="^model\sname\s*:\s(.*)$"
    rgxp_FAMILY="^cpu\sfamily\s*:\s(.*)$"
    rgxp_MODEL="^model\s*:\s(.*)$"
    rgxp_STEPPING="^stepping\s*:\s(.*)$"
    rgxp_MICROCODE="^microcode\s*:\s(.*)$"
    rgxp_MHZ="^cpu\sMHz\s*:\s(.*)$"
    rgxp_FPU="^fpu\s*:\s(.*)$"
    rgxp_FPUEXCEPTION="^fpu_exception\s*:\s(.*)$"
    rgxp_WP="^wp\s*:\s(.*)$"
    rgxp_FLAGS="^flags\s*:\s(.*)$"
    rgxp_ADDR="^address\ssizes\s*:\s(.*)$"
    rgxp_POWERM="^power\smanagement\s*:\s(.*)$"
        if [[ "$line" =~ $rgxp_PROCNUM ]] ; then 
            INFO_PROCNUM_PROC_CPUINFO="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_NAME ]] ; then 
            INFO_NAME_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_NAME_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_FAMILY ]] ; then 
            INFO_FAMILY_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_FAMILY_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_MODEL ]] ; then 
            INFO_MODEL_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_MODEL_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_STEPPING ]] ; then 
            INFO_STEPPING_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_STEPPING_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_MICROCODE ]] ; then 
            INFO_MICROCODE_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_MICROCODE_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_MHZ ]] ; then 
            INFO_MHZ_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_MHZ_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_FPU ]] ; then 
            INFO_FPU_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_FPU_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_FPUEXCEPTION ]] ; then 
            INFO_FPUEXCEPTION_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_FPUEXCEPTION_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_WP ]] ; then 
            INFO_WP_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_WP_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_FLAGS ]] ; then 
            INFO_FLAGS_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_FLAGS_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_ADDR ]] ; then 
            INFO_ADDR_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_ADDR_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        elif [[ "$line" =~ $rgxp_POWERM ]] ; then 
            INFO_POWERM_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]="${BASH_REMATCH[1]}"
            #echo ">>>>>>>>" ${INFO_POWERM_PROC_CPUINFO[ $INFO_PROCNUM_PROC_CPUINFO ]} $INFO_PROCNUM_PROC_CPUINFO
        fi
done < /proc/cpuinfo
#echo E
#Testa se i processori non sono uguali. Il batch è concepito per riportare inforamzioni corrette solo se tutti le cpu sono uguali. Se non lo sono bisogna aggiungere la gestione di quel caso
PRIMO=${INFO_NAME_PROC_CPUINFO[0]}
for el in "${INFO_NAME_PROC_CPUINFO[@]}"
do
    if [ "$PRIMO" != "$el" ]; then 
        echo "UN PROCESSORE E' DIVERSO DAGLI ALTRI. CASO NON GESTITO"
        echo "UN PROCESSORE E' DIVERSO DAGLI ALTRI. CASO NON GESTITO" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
        exit $PROCESSORIMISTI
    fi
done

echo "- - CAT PROC CPUINFO - -" >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "NAME         >" ${INFO_NAME_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "FAMILY       >" ${INFO_FAMILY_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "MODEL        >" ${INFO_MODEL_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "STEPPING     >" ${INFO_STEPPING_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "MICROCODE    >" ${INFO_MICROCODE_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "MHZ          >" ${INFO_MHZ_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "FPU          >" ${INFO_FPU_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "FPUEXCEPTION >" ${INFO_FPUEXCEPTION_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "WP           >" ${INFO_WP_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "FLAGS        >" ${INFO_FLAGS_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "ADDR         >" ${INFO_ADDR_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"
echo "POWERM       >" ${INFO_POWERM_PROC_CPUINFO[ 0 ]} >>"${BENCHDIR}/repo/${LOGDATE}_RawOut_proc_cpuinfo.log"


#Ritorana il primo argomento valorizzato sulla lista
function primovalore {
    local buono
    buono="unknown"
    while test $# -gt 0
    do
        if [ ! "$1" == "" ]; then
            buono=$1
            break
        fi
        shift
    done
    echo $buono
}

#$INFO_TOT_THREAD 
#echo F
CPU_NAME=$(primovalore "${INFO_NAME_PROC_CPUINFO[ 0 ]}" "$INFO_NAME_DMIDECODE" "$INFO_NAME_X86INFO")
CPU_DESC=$(primovalore "$INFO_SYS_X86INFO" "")
CPU_ID=$(primovalore "$INFO_ID_DMIDECODE")
CPU_SCK=$(primovalore "$INFO_SOKET_DMIDECODE")
CPU_NUMCORE=$(primovalore "$INFO_CORE_DMIDECODE" "1")
tmp=`echo "$INFO_TOT_THREAD / $CPU_NUMCORE" |bc`
CPU_THREADxCORE=$(primovalore "$tmp")
#tmp=`echo "$CPU_NUMCORE * $CPU_THREADxCORE" |bc`
CPU_THREAD=$(primovalore $INFO_TOT_THREAD ) #non uso i valori da dmidecode. mi sembra riporti solo i thread attivi al momento del test
#if [ "$INFO_VOLTAGE_DMIDECODE" == "Unknow" ]; then
#    CPU_VOLT = "?"
#fi
CPU_VOLT=$(primovalore "$INFO_VOLTAGE_DMIDECODE")
CPU_EFAMILY=$(primovalore "$INFO_EFAMILY_X86INFO" "") #questi sono solo in x86info e forse in $INFO_EFAMILY_DMIDECODE
CPU_EMODEL=$(primovalore "$INFO_EMODEL_X86INFO" "") #questi sono solo in x86info e forse in $INFO_TYPE_DMIDECODE
CPU_FAMILY=$(primovalore "${INFO_FAMILY_PROC_CPUINFO[ 0 ]}" "$INFO_FAMILY_DMIDECODE" "$INFO_FAMILY_X86INFO" "")
CPU_MODEL=$(primovalore "${INFO_MODEL_PROC_CPUINFO[ 0 ]}" "$INFO_MODEL_DMIDECODE" "$INFO_MODEL_X86INFO" "")
CPU_STEPPING=$(primovalore "${INFO_STEPPING_PROC_CPUINFO[ 0 ]}" "$INFO_STEPPING_DMIDECODE" "$INFO_STEPPING_X86INFO" "")
CPU_MICROCODE=$(primovalore "${INFO_MICROCODE_PROC_CPUINFO[ 0 ]}" "") 
CPU_MHZ=$(primovalore "${INFO_MHZ_PROC_CPUINFO[ 0 ]}" "$INFO_CPUMHZ_LSCPU") 
CPU_MAXMHZ=$(primovalore "$INFO_MAXSPEED_DMIDECODE")
CPU_EXTCLOCK=$(primovalore "$INFO_ECLOCK_DMIDECODE")
CPU_L1DCACHE=$(primovalore "$INFO_L1DCACHE_LSCPU" "")
CPU_L1ICACHE=$(primovalore "$INFO_L1ICACHE_LSCPU" "")
CPU_L2CACHE=$(primovalore "$INFO_L2CACHE_LSCPU" "")
CPU_L3CACHE=$(primovalore "$INFO_L3CACHE_LSCPU" "")
CPU_VTX=$(primovalore "$INFO_VIRTUALIZATION_LSCPU")
FPU_PRESENT=$(primovalore "${INFO_FPU_PROC_CPUINFO[ 0 ]}" "N")
FPU_EXCEPTION=$(primovalore "${INFO_FPUEXCEPTION_PROC_CPUINFO[ 0 ]}" "N")
CPU_WRITEPROTECT=$(primovalore "${INFO_WP_PROC_CPUINFO[ 0 ]}" "N")
CPU_LONG_NOPS=$(primovalore "$INFO_LONG_NOPS_X86INFO")
CPU_ADDR=$(primovalore "${INFO_ADDR_PROC_CPUINFO[ 0 ]}")
CPU_POWERMANAGER=$(primovalore "${INFO_POWERM_PROC_CPUINFO[ 0 ]}")
CPU_FLAGS=$(primovalore "${INFO_FLAGS_PROC_CPUINFO[ 0 ]}")

echo " " >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "********************************************* CPU INFO *********************************************" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
#echo "-------" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Modello CPU       :: @CPU_NAME         = $CPU_NAME" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Desc. aggiuntiva  :: @CPU_DESC         = $CPU_DESC" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Identificativo    :: @CPU_ID           = $CPU_ID" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "soket             :: @CPU_SCK          = $CPU_SCK" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Num. Core fisici  :: @CPU_NUMCORE      = $CPU_NUMCORE" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Thread per core   :: @CPU_THREADxCORE  = $CPU_THREADxCORE" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Tot Thread logici :: @CPU_THREAD       = $CPU_THREAD" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Voltaggio CPU     :: @CPU_VOLT         = $CPU_VOLT" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CPU Ext FAMILY    :: @CPU_EFAMILY      = $CPU_EFAMILY" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CPU Ext MODEL     :: @CPU_EMODEL       = $CPU_EMODEL" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CPU FAMILY        :: @CPU_FAMILY       = $CPU_FAMILY" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CPU MODEL         :: @CPU_MODEL        = $CPU_MODEL" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CPU STEPPING      :: @CPU_STEPPING     = $CPU_STEPPING" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "MICROCODE         :: @CPU_MICROCODE    = $CPU_MICROCODE" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "MHZ               :: @CPU_MHZ          = $CPU_MHZ" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "MAXMHZ            :: @CPU_MAXMHZ       = $CPU_MAXMHZ" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "External CLOCK    :: @CPU_EXTCLOCK     = $CPU_EXTCLOCK" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CACHE L1D         :: @CPU_L1DCACHE     = $CPU_L1DCACHE" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CACHE L1I         :: @CPU_L1ICACHE     = $CPU_L1ICACHE" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CACHE L2          :: @CPU_L2CACHE      = $CPU_L2CACHE" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "CACHE L3          :: @CPU_L3CACHE      = $CPU_L3CACHE" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Virtualiz. Tech.  :: @CPU_VTX          = $CPU_VTX" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Cooproc.Mat. (FPU):: @FPU_PRESENT      = $FPU_PRESENT" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "FPU EXCEPTION     :: @FPU_EXCEPTION    = $FPU_EXCEPTION" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "WRITE PROTECT     :: @CPU_WRITEPROTECT = $CPU_WRITEPROTECT" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "LONG NOPS         :: @CPU_LONG_NOPS    = $CPU_LONG_NOPS" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Address mode      :: @CPU_ADDR         = $CPU_ADDR" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Power Manager     :: @CPU_POWERMANAGER = $CPU_POWERMANAGER" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "FLAGS             :: @CPU_FLAGS        = $CPU_FLAGS" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
#echo G
echo >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
echo "********************************************* RAM INFO *********************************************" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"

function getmembank {
    dmidecode --type memory |gawk '
    BEGIN {
        devicenum=0
        banchi=0
        OFS="§"
    }
        
    (/^Memory\sDevice/){   
        devicenum++
        #print devicenum,$0
    }
    (/^\s*Size:/){
        #print devicenum,$0
        asize[devicenum]=$2
        asizeunit[devicenum]=$3
        if ( (asize[devicenum]>0) && (asize[devicenum]!="No") ){
                banchi++   
            }
        }
    (/^\s*Type:/){
        #print devicenum,$0
        atype[devicenum]=$2
        }
    (/^\s*Locator:/){
        #print devicenum,$0
        apos[devicenum]=$2$3$4
        }
    (/^\s*Speed:/){
        #print devicenum,$0
        aspeed[devicenum]=$2
        aspeedunit[devicenum]=$3
        }
    (/^\s*Manufacturer:/){
        #print devicenum,$0
        amanufacture[devicenum]=$2$3$4$5
        }
    (/^\s*Part\sNumber:/){
        #print devicenum,$0
        amodel[devicenum]=$3$4$5
        }
    (/^\s*Configured\sClock\sSpeed:/){
        #print devicenum,$0
        accspeed[devicenum]=$4
        accspeedunit[devicenum]=$5
        }
    END{
        banco=1 
        print "TotBanchi "banchi
        for ( i = 1; i <= devicenum; i++ ){
			#print "XX"asize[i]"XX"
            if ( (asize[i]>0)  && (asize[i]!="No") ){
					print banco,apos[i],asize[i], asizeunit[i], atype[i],aspeed[i],aspeedunit[i],amanufacture[i],amodel[i],accspeed[i],accspeedunit[i]
					banco++
            }
        }
    }
  '
}
function decodeinfomembanc {
OLDIFS=$IFS
rgxp_TotBanchi="^TotBanchi\s+([[:digit:]]*)"
Tot_Banchi=0
oldbanco=0
while read -r line; do
    
    #IFS=- read inutile Tot_Banchi <<< line
    if [[ "$line" =~ $rgxp_TotBanchi ]] ; then
        Tot_Banchi="${BASH_REMATCH[1]}"
        echo "Totale benchi di memoria    :: @Tot_Banchi        = $Tot_Banchi" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
        
    else
        
        IFS=§
        set $line
        if [ $oldbanco -ne $1 ]; then
            echo >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
            oldbanco=$1
        fi
        eval mem_bankpos_$1='"'$2'"'
        eval mem_banksize_$1='"'$3 $4'"'
        eval mem_banktype_$1='"'$5'"'
        eval mem_bankspeed_$1='"'$6 $7'"'
        eval mem_manufacture_$1="$8"
        eval mem_model_$1='"'$9'"'
        eval mem_clock_$1='"'${10} ${11}'"'
        
        echo "Nome Slot del banco         :: @mem_bankpos_$1     = `eval echo '$mem_bankpos_'$1`" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
        echo "Dimensione del banco        :: @mem_banksize_$1    = `eval echo '$mem_banksize_'$1`" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
        echo "Tipo di memoria             :: @mem_banktype_$1    = `eval echo '$mem_banktype_'$1`" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
        echo "Velocità del banco          :: @mem_bankspeed_$1   = `eval echo '$mem_bankspeed_'$1`" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
        echo "Marca                       :: @mem_manufacture_$1 = `eval echo '$mem_manufacture_'$1`" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
        echo "Modello                     :: @mem_model_$1       = `eval echo '$mem_model_'$1`" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
        echo "Velocità di clock impostata :: @mem_clock_$1       = `eval echo '$mem_clock_'$1`" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
    fi
done < <(getmembank)
#getmembank
IFS=$OLDIFS
}





mem_total=$(cat /proc/meminfo |grep MemTotal|awk '{print $2}')
mem_avaiable=$(free --total -k |awk  '(/Mem:/){print $2}')
mem_used=$(free --total -k |awk  '(/Mem:/){print $3}')
mem_free=$(free --total -k |awk  '(/Mem:/){print $4}')
swap_avaiable=$(free --total -k |awk  '(/Swap:/){print $2}')
swap_used=$(free --total -k |awk  '(/Swap:/){print $3}')
swap_free=$(free --total -k |awk  '(/Swap:/){print $4}')

echo "Memoria Totale in kb        :: @mem_total         = $mem_total" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Memoria disponibile in kb   :: @mem_avaiable      = $mem_avaiable" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Memoria utilizzata in kb    :: @mem_used          = $mem_used" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Memoria libera in kb        :: @mem_free          = $mem_free" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Swap disponibile in kb      :: @swap_avaiable     = $swap_avaiable" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Swap utilizzata in kb       :: @swap_used         = $swap_used" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Swap libera in kb           :: @swap_free         = $swap_free" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"

decodeinfomembanc
#exit 0
echo >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
echo "********************************************* VGA INFO *********************************************" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"




function info_lspci {
    local bussvideo=$1
    local numsv=$2
    videobus[$numsv]=$1
    echo "info_lspci $bussvideo -- $numsv -- ${videobus[$numsv]}"
    
    
    rgxp_SVvendor="^\s*Subsystem:\s+(.*)"
    rgxp_SVflags="^\s*Flags:\s+(.*)"
    rgxp_SVdriver="^\s*Kernel\sdriver\sin\suse:\s+(.*)"
    rgxp_SVmodule="^\s*Kernel\smodules:\s+(.*)"
    while read -r line; do
        if [[ "$line" =~ $rgxp_SVvendor ]] ; then
            val_SVvendor[$numsv]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVflags ]] ; then
            val_SVflags[$numsv]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVdriver ]] ; then
            val_SVdriver[$numsv]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVmodule ]] ; then
            val_SVmodule[$numsv]="${BASH_REMATCH[1]}"
        fi
        
    done < <(lspci -v -s $bussvideo)
}

function infomemoryvideo {
    local bussvideo=$1
    local numsv=$2
    echo "infomemoryvideo $bussvideo $numsv"
    echo "[[[ @infoMEMVGAmemo_${numsv}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    sudo lspci -v -s $bussvideo |grep "Memory\sat\s" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
    echo "]]]" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
}
    
function getinfovideo {
    rgxp_SVdescvgatype="^\s*description:\s+(.*)"
    rgxp_SVproduct="^\s*product:\s+(.*)"
    rgxp_SVchipsetVendor="^\s*vendor:\s+(.*)"
    rgxp_SVversion="^\s*version:\s+(.*)"
    rgxp_SVbusWidth="^\s*width:\s+(.*)"
    rgxp_SVclock="^\s*clock:\s+(.*)"
    rgxp_SVcapabilities="^\s*capabilities:\s+(.*)"
    count_SV_lshw=-1
    
    while read -r line; do
        if [[ "$line" =~ $rgxp_SVdescvgatype ]] ; then
            count_SV_lshw=$(($count_SV_lshw+1))
            j=$(($count_SV_lshw+1))
            echo $(lspci |grep VGA | awk '{print $1}'|sed -n "$j p")
            bsvideo=$(lspci |grep VGA | awk '{print $1}'|sed -n "$j p")
            echo "dddd $bsvideo $count_SV_lshw"
            info_lspci $bsvideo $count_SV_lshw 
            val_SVdescvgatype[$count_SV_lshw]="${BASH_REMATCH[1]}"
            
        elif [[ "$line" =~ $rgxp_SVproduct ]] ; then
            val_SVproduct[$count_SV_lshw]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVchipsetVendor ]] ; then
            val_SVchipsetVendor[$count_SV_lshw]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVversion ]] ; then
            val_SVversion[$count_SV_lshw]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVbusWidth ]] ; then
            val_SVbusWidth[$count_SV_lshw]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVclock ]] ; then
            val_SVclock[$count_SV_lshw]="${BASH_REMATCH[1]}"
        elif [[ "$line" =~ $rgxp_SVcapabilities ]] ; then
            val_SVcapabilities[$count_SV_lshw]="${BASH_REMATCH[1]}"
        fi
    done < <(lshw -C display)
 
    for i in `seq 0 $count_SV_lshw`; do
    j=$(($i+1))
    #echo ":: @ = $val_SVdescvgatype" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Scheda video           [$j]:: @val_SVproduct_${j}         = ${val_SVproduct[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Versione               [$j]:: @val_SVversion_${j}         = ${val_SVversion[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Produttore             [$j]:: @val_SVvendor_${j}          = ${val_SVvendor[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Produttore chipset     [$j]:: @val_SVchipsetVendor_${j}   = ${val_SVchipsetVendor[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Ampiezza di bus        [$j]:: @val_SVbusWidth_${j}        = ${val_SVbusWidth[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Velocità di clock      [$j]:: @val_SVclock_${j}           = ${val_SVclock[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Capacità               [$j]:: @val_SVcapabilities_${j}    = ${val_SVcapabilities[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Flags                  [$j]:: @val_SVflags_${j}           = ${val_SVflags[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Driver utilizzati      [$j]:: @val_SVdriver_${j}          = ${val_SVdriver[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "Modulo kernel          [$j]:: @val_SVmodule_${j}          = ${val_SVmodule[$i]}" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
    echo "----------------------- ${videobus[$numsv]}"
    infomemoryvideo ${videobus[$i]} $j
    done
}

#d
val_SVopenglversion=$(glxinfo|grep "OpenGL version string"|sed 's/OpenGL version string: //')
val_SVopenglvendorstring=$(glxinfo|grep "OpenGL vendor string"|sed 's/OpenGL vendor string: //')
val_SVopenglrendererstring=$(glxinfo|grep "OpenGL renderer string"|sed 's/OpenGL renderer string: //')

echo "Versione OpenGL   [ATTIVA]:: @val_SVopenglversion        = $val_SVopenglversion" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Info OpenGL Vendor[ATTIVA]:: @val_SVopenglvendorstring   = $val_SVopenglvendorstring" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "OpenGL renderer   [ATTIVA]:: @val_SVopenglrendererstring = $val_SVopenglrendererstring" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
getinfovideo

echo "********************************************* BOARD INFO *********************************************" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
function getboardinfo {
    rgxp_BOARD_manufacturer="^Manufacturer:\s+(.*)"
rgxp_BOARD_model="^Product\sName:\s+(.*)"
rgxp_BOARD_revision="^Version:\s+(.*)"

while read -r line; do
    if [[ "$line" =~ $rgxp_BOARD_manufacturer ]] ; then
        BOARD_manufacturer="${BASH_REMATCH[1]}"
    elif [[ "$line" =~ $rgxp_BOARD_model ]] ; then
        BOARD_model="${BASH_REMATCH[1]}"
    elif  [[ "$line" =~ $rgxp_BOARD_revision ]] ; then
        BOARD_revision="${BASH_REMATCH[1]}"
    #else
        #echo $line
    fi
done< <(dmidecode --type baseboard)
rgxp_BIOS_vendor="^Vendor:\s+(.*)"
rgxp_BIOS_version="^Version:\s+(.*)"
rgxp_BIOS_date="^Release\sDate:\s+(.*)"

while read -r line; do
    if [[ "$line" =~ $rgxp_BIOS_vendor ]] ; then
        BIOS_vendor="${BASH_REMATCH[1]}"
    elif [[ "$line" =~ $rgxp_BIOS_version ]] ; then
        BIOS_version="${BASH_REMATCH[1]}"
    elif  [[ "$line" =~ $rgxp_BIOS_date ]] ; then
        BIOS_date="${BASH_REMATCH[1]}"
    #else
        #echo $line
    fi
done< <(dmidecode --type bios)

echo "Marca   della scheda madre   :: @MB_manufacturer = $BOARD_manufacturer" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "modello della scheda madre   :: @MB_model        = $BOARD_model" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "revisione della scheda madre :: @MB_revision     = $BOARD_revision" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Bios                         :: @BIOS_vendor     = $BIOS_vendor" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Versione del bios            :: @BIOS_version    = $BIOS_version" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Data del bios                :: @BIOS_date       = $BIOS_date" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
}
getboardinfo
echo >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log" 
echo "********************************************* xBUNTU INFO *********************************************" >>"${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
distroname=`lsb_release -ds`
kenelinfo=`uname -siro`
echo "Distribuzione linux          :: @LinuxDistroName = $distroname" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
echo "Kernel                       :: @LinuxKenelInfo  = $kenelinfo" >> "${BENCHDIR}/repo/${LOGDATE}_HARDWARE.log"
