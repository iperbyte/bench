function calltestmd5 {
    echo "sono qua $1" 
    xterm -geometry 200x25+500+50 -e nice -n -20 `eval echo $1`

}

function start_md5benc {
    if [ $BENCHMD5 -ne 0 ]; then
        if [ $BENCHMD5_PY -ne 0 ]; then
            #prepare_hardinfo_value
            if [ $BENCHMD5_PY_LIN -ne 0 ]; then
                svuotamemoria "benchmd5_py_seriale"
                echo "Test hasing md5,sha1,sha224,sha246,sha284,sha512 - $BENCHMD5_PY_LIN cicli seriali"
                singletest calltestmd5 "${BENCHDIR}/tmp/benchmd5_py_mono.sh"
                echo "Tempo per esecuzione in serie di $BENCHMD5_PY_LIN benchmd5.py                       (basso è meglio) = $singletest_dft"
                echo "Tempo per esecuzione in serie di $BENCHMD5_PY_LIN benchmd5.py                       (basso è meglio) :: @hasing_py_lin   = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            else
                echo "skip Test hasing benchmd5.py seriale"
            fi
            prepare_hardinfo_value
            if [ $BENCHMD5_PY_PAR -ne 0 ]; then
                svuotamemoria "benchmd5_py_multithread"
                echo "Test hasing md5,sha1,sha224,sha246,sha284,sha512 - $BENCHMD5_PY_LIN cicli in multithread"
                singletest calltestmd5 "${BENCHDIR}/tmp/benchmd5_py_multi.sh"
                echo "Tempo per esecuzione in multithread di $BENCHMD5_PY_PAR benchmd5.py                (basso è meglio) = $singletest_dft"
                echo "Tempo per esecuzione in multithread di $BENCHMD5_PY_PAR benchmd5.py                (basso è meglio) :: @hasing_py_par   = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            else
                echo "skip Test hasing benchmd5.py multithread"
            fi
        else
            prepare_hardinfo_value
            echo "skip Test hasing in python"
        fi
        if [ $BENCHMD5_PL -ne 0 ]; then
            prepare_hardinfo_value
            if [ $BENCHMD5_PL_LIN -ne 0 ]; then
                svuotamemoria "benchmd5_pl_seriale"
                echo "Test hasing md5,sha1,sha224,sha246,sha284,sha512 - $BENCHMD5_PL_LIN cicli seriali"
                singletest calltestmd5 "${BENCHDIR}/tmp/benchmd5_pl_mono.sh"
                echo "Tempo per esecuzione in serie di $BENCHMD5_PL_LIN benchmd5.pl                       (basso è meglio) = $singletest_dft"
                echo "Tempo per esecuzione in serie di $BENCHMD5_PL_LIN benchmd5.pl                       (basso è meglio) :: @hasing_pl_lin   = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            else
                echo "skip Test hasing benchmd5.py seriale"
            fi
            prepare_hardinfo_value
            if [ $BENCHMD5_PL_PAR -ne 0 ]; then
                svuotamemoria "benchmd5_pl_multithread"
                echo "Test hasing md5,sha1,sha224,sha246,sha284,sha512 - $BENCHMD5_PL_LIN cicli in multithread"
                singletest calltestmd5 "${BENCHDIR}/tmp/benchmd5_pl_multi.sh"
                echo "Tempo per esecuzione in multithread di $BENCHMD5_PL_PAR benchmd5.pl                (basso è meglio) = $singletest_dft"
                echo "Tempo per esecuzione in multithread di $BENCHMD5_PL_PAR benchmd5.pl                (basso è meglio) :: @hasing_pl_par   = $singletest_dft" >> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            else
                echo "skip Test hasing benchmd5.py multithread"
            fi
        else
            prepare_hardinfo_value
            echo "skip Test hasing in perl"
        fi
    else
        prepare_hardinfo_value
        prepare_hardinfo_value
        echo "skip Test di  hasing"
    fi
}
function shsthfhstart_md5benc1 {
if [ $BENCHMD5 -ne 0 ]; then
    if [ $BENCHMD5_PY -ne 0 ]; then
        prepare_hardinfo_value
        svuotamemoria "benchmd5_py_seriale"
        if [ $BENCHMD5_PY_LIN -ne 0 ]; then
            timemultitest 0 $BENCHMD5_PY_LIN ./benchmd5.py hasing_py_lin
        fi
        prepare_hardinfo_value
        svuotamemoria "benchmd5_py_multithread"
        if [ $BENCHMD5_PY_PAR -ne 0 ]; then
            timemultitest 1 $BENCHMD5_PY_PAR ./benchmd5.py hasing_py_par
        fi
    fi
    if [ $BENCHMD5_PL -ne 0 ]; then
        prepare_hardinfo_value
        svuotamemoria "benchmd5_pl_seriale"
        if [ $BENCHMD5_PL_LIN -ne 0 ]; then
            timemultitest 0 $BENCHMD5_PL_LIN ./benchmd5.pl hasing_pl_lin
        fi
        prepare_hardinfo_value
        svuotamemoria "benchmd5_pl_multithread"
        if [ $BENCHMD5_PL_PAR -ne 0 ]; then
            timemultitest 1 $BENCHMD5_PL_PAR ./benchmd5.pl hasing_pl_par
        fi
    fi
fi
}
 
