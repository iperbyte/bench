
function start_pyrit_test {
    if [ $PYRIT_test -ne 0 ]; then
        svuotamemoria "pyrit"
        echo xterm -l -lf xterm.pyrit.log -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 `echo $SETPYRIT_TEST` 2>&1 | tee /tmp/pippo.txt
        xterm -l -lf "${BENCHDIR}/tmp/xterm.pyrit.log" -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 `echo $SETPYRIT_TEST`
        rgxp_PYRITPMKs="^Computed\s+([1234567890\.]*)\s+PMKs.*$"
        while read -r line; do
            if [[ "$line" =~ $rgxp_PYRITPMKs ]] ; then
                VAL_PYRITPMKs="${BASH_REMATCH[1]}"
                echo "pyrit test PYRITPMKs                                                  (alto  è meglio) = $VAL_PYRITPMKs"
                echo "pyrit test PYRITPMKs                                                  (alto  è meglio) :: @PYRITPMKs       = $VAL_PYRITPMKs" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            fi
        done < "${BENCHDIR}/tmp/xterm.pyrit.log"
    else
        echo "skip PYRIT_test"
    fi
}



#VAL_PYRITPMKs

 
