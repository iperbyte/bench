MEMTESTPID=0
####funzione che elabora la differenza fra 2 date lette con  date "%s%N" 
function svuotamemoria {
    MEMTESTNAME=$1
    #Stoppa il monitor precedente
    if [ $MEMTESTPID -ne 0 ]; then 
        disown $MEMTESTPID
        kill -9 $MEMTESTPID
    fi
    sysctl -w vm.drop_caches=3 >/dev/null
    sleep 0.5 #aspetta che la memoria si sia liberata
    free -k -s1 >"${BENCHDIR}/tmp/mem_monitor_${MEMTESTNAME}.log" &
    #esegue almeno 2 cicli pieni di lettura a vuoto
    sleep 3
    MEMTESTPID=$!
}
function ferma_monitor_memoria {
#Stoppa l'ultimo programma di monitor della memoria
if [ $MEMTESTPID -ne 0 ]; then 
    disown $MEMTESTPID
    kill -9 $MEMTESTPID
fi
}



function difftime {
            diff=$((($1-$2)/1000000))
            sec=$(($diff/1000))
            milli=$(($diff%1000))
            min=$(($sec/60))
            sec=$(($sec%60))
            h=$(($min/60))
            min=$(($min%60))
            echo $(printf "%02d:%02d.%02d.%03d" $h $min $sec $milli)
           }
           
function singletest {
    #xterm -geometry 200x25+500+50 -e tail -f "${BENCHDIR}"/tmp/bench.log &
    #local pidterm=$!
    local testprog=$1
    local date1=$(date +"%s%N")
    echo "Eseguo test: $1 $2 $3 $4 $5 $6 $7 $8 $9"
    #msg=$(eval "$1" '"'$2'"' '"'$3'"' '"'$4'"' '"'$5'"' '"'$6'"' '"'$7'"' '"'$8'"' '"'$9'"')
    #echo xxxx $testprog '"'$2'"' '"'$3'"' '"'$4'"' '"'$5'"' '"'$6'"' '"'$7'"' '"'$8'"' '"'$9'"'
    eval $testprog '"'$2'"' '"'$3'"' '"'$4'"' '"'$5'"' '"'$6'"' '"'$7'"' '"'$8'"' '"'$9'"'
    local date2=$(date +"%s%N")
    local dft=$(difftime $date2 $date1)
    #echo Test: $msg Eseguito in: $dft
    #echo Test: $msg Eseguito in: $dft 1>> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
    singletest_dft=$dft
    #disown $pidterm
    #kill -9 $pidterm 
}

function timemultitest {
    local parallel=$1
    local numcicli=$2
    local testprog=$3
    local testvar=$4
    xterm -geometry 200x25+500+50 -e tail -f "${BENCHDIR}"/tmp/bench.log &
    local pidterm=$!
    local date1=$(date +"%s%N")
    #seq 20 xarg nice -n -20 ./benchmd5.py
    if [ $parallel -eq 0 ]; then #se $parallel è 0 esegue i test in modo lineare
        echo single core >>"${BENCHDIR}"/tmp/bench.log;
        for i in `seq $numcicli` ; do $testprog $i >>"${BENCHDIR}"/tmp/bench.log; done
        modo="LINEARE       "
    else #eltrimenti li carica in parallelo
        echo multicore core >>"${BENCHDIR}"/tmp/bench.log;
        echo seq $numcicli parallel nice -n -20 $testprog
        seq $numcicli |parallel --gnu --nice -20 $testprog >>"${BENCHDIR}"/tmp/bench.log
        modo="PARALLELO SU $INFO_TOT_THREAD CORE"
    fi
    local date2=$(date +"%s%N")
    local dft=$(difftime $date2 $date1)
    disown $pidterm
    kill -9 $pidterm 
    echo "Tempo per esecuzione $modo di $numcicli $testprog (basso è meglio) :: $dft"
    echo "Tempo per esecuzione $modo di $numcicli $testprog (basso è meglio) :: @${testvar} = $dft" 1>> "${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
}

 
