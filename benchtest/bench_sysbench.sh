
function start_sysbench_test {
    if [ $SYSBENCH -ne 0 ]; then
        svuotamemoria "sysbench"
        local test="${BENCHDIR}/tmp/bench_sysbench.sh"
        #echo $test
        #echo xterm -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 `echo $test`
        xterm -geometry 200x25+500+50 -fa curier -fs 20 -bg black -fg white -e nice -n -20 `echo $test`
        rgxp_SYSB="^total time:\s+([1234567890\.]*s).*$"
        while read -r line; do
            if [[ "$line" =~ $rgxp_SYSB ]] ; then
                VAL_SYSBENCH="${BASH_REMATCH[1]}"
                echo "Secondi test SYSBENCH su $INFO_TOT_THREAD Thread                                     (basso è meglio) = $VAL_SYSBENCH"
                echo "Secondi test SYSBENCH su $INFO_TOT_THREAD Thread                                     (basso è meglio) :: @val_sysbench    = $VAL_SYSBENCH" >>"${BENCHDIR}"/repo/${LOGDATE}_benchresult.txt
            fi
        done < "${BENCHDIR}/repo/${LOGDATE}_RawOut_sysbench.log"
    else
        echo "skip SYSBENCH"
    fi
}


 
