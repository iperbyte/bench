#echo "entro in bench_createenvdir"
#Crea la dir del test se non esiste

function safe_delete_file {
    todelete=$1
    if [ -e "${todelete}" ]; then
        rm -f "${todelete}"
    fi
}

if [ ! -d "${BENCHDIR}" ] ; then
    mkdir -p "${BENCHDIR}"
    chown $SUDO_USER ${BENCHDIR}
    chgrp $SUDO_GRP  ${BENCHDIR}
fi
if [ ! -d "${BENCHDIR}/data" ] ; then
    mkdir -p "${BENCHDIR}/data"
    chown $SUDO_USER "${BENCHDIR}/data"
    chgrp $SUDO_GRP  "${BENCHDIR}/data"
fi
if [ ! -d "${BENCHDIR}/tmp" ] ; then
    mkdir -p "${BENCHDIR}/tmp"
    chown $SUDO_USER "${BENCHDIR}/tmp"
    chgrp $SUDO_GRP  "${BENCHDIR}/tmp"
fi
if [ ! -d "${BENCHDIR}/repo" ] ; then
    mkdir -p "${BENCHDIR}/repo"
    chown $SUDO_USER "${BENCHDIR}/repo"
    chgrp $SUDO_GRP  "${BENCHDIR}/repo"
fi
#Salva tutto l'output dentro ad un file di log. (non serve al programma ma solo durante lo sviluppo)
exec > >(tee -a "${BENCHDIR}/repo/${LOGDATE}_bench_rawout.log") 2>&1
#per comodita lo rende 777 a tutti, così alla fine è facile cancellarli senza essere root.
sleep 0.1
chown $SUDO_USER "${BENCHDIR}/repo/${LOGDATE}_bench_rawout.log"
chgrp $SUDO_GRP  "${BENCHDIR}/repo/${LOGDATE}_bench_rawout.log"


#GENERATORE RANDOM tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1
#CHIAMANTEbench_sh="TgR5iByg3k4NLPVvOvfViFi8dDEOz2"
#. ./hwdetect.sh            
#echo "passo1"

source $(dirname $0)/benchtest/bench_hdetect.sh
#exit 0
#echo "passo2"
#Il file dove saranno salvati i risultati dei benchmark

BENCHRESULT="${BENCHDIR}/repo/${LOGDATE}_benchresult.txt"

touch "$BENCHRESULT"
chown $SUDO_USER "$BENCHRESULT"
chgrp $SUDO_GRP  "$BENCHRESULT"

#Colori per i messaggi evidenziati
IGreen='\033[0;92m'       # Verde
BIGreen="\033[1;92m"      # Verde

if [ -e "${BENCHDIR}/tmp/ToS-4k-1920_z9.7z" ]; then
    rm -f "${BENCHDIR}/tmp/ToS-4k-1920_z9.7z"
fi
if [ -e "${BENCHDIR}/tmp/ToS-4k-1920_z9.faketest.7z" ]; then
    rm -f "${BENCHDIR}/tmp/ToS-4k-1920_z9.faketest.7z"
fi
if [ -e "${BENCHDIR}/tmp/ToS-4k-1920_z9.lrz" ]; then
    rm -f "${BENCHDIR}/tmp/ToS-4k-1920_z9.lrz"
fi
if [ -e "${BENCHDIR}/tmp/ToS-4k-1920_z9.faketest.lrz" ]; then
    rm -f "${BENCHDIR}/tmp/ToS-4k-1920_z9.faketest.lrz"
fi

if [ ! -e "${BENCHDIR}/data/ToS-4k-1920.mov" ]; then
    #http://mango.blender.org/download/
    #http://ftp.halifax.rwth-aachen.de/blender/demo/movies/ToS/ToS-4k-1920.mov (Mirror tedesco)
    wget --directory-prefix="${BENCHDIR}/data" http://ftp.halifax.rwth-aachen.de/blender/demo/movies/ToS/ToS-4k-1920.mov
    chown $SUDO_USER "${BENCHDIR}/data/ToS-4k-1920.mov"
    chgrp $SUDO_GRP  "${BENCHDIR}/data/ToS-4k-1920.mov"
fi

#if [ ! -e "${BENCHDIR}/data/i7z_64bit" ]; then
#    wget --directory-prefix="${BENCHDIR}/data" http://i7z.googlecode.com/svn/trunk/i7z_64bit
#    chown $SUDO_USER "${BENCHDIR}/data/i7z_64bit"
#    chgrp $SUDO_GRP  "${BENCHDIR}/data/i7z_64bit"
#    chmod +x "${BENCHDIR}/data/i7z_64bit"
#fi

if [ -e "${BENCHDIR}/tmp/xterm.pyrit.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.pyrit.log"
fi

if [ -e "${BENCHDIR}/tmp/xterm.pyrit.tee.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.pyrit.tee.log"
fi
if [ -e "${BENCHDIR}/tmp/xterm.7zip.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.7zip.log"
fi
if [ -e "${BENCHDIR}/tmp/xterm.7zip_b.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.7zip_b.log"
fi

if [ -e "${BENCHDIR}/tmp/xterm.hardinfo.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.hardinfo.log"
fi
if [ -e "${BENCHDIR}/tmp/xterm.turbostat_check.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.turbostat_check.log"
fi
if [ -e "${BENCHDIR}/tmp/xterm.turbostat_riposo.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.turbostat_riposo.log"
fi
if [ -e "${BENCHDIR}/tmp/xterm.turbostat_1Core.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.turbostat_1Core.log"
fi

if [ -e "${BENCHDIR}/tmp/xterm.turbostat_Full_Core.log" ]; then
    rm -f "${BENCHDIR}/tmp/xterm.turbostat_Full_Core.log"
fi

safe_delete_file "${BENCHDIR}/tmp/mem_monitor_sysbench.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_benchmd5_py_seriale.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_benchmd5_py_multithread.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_benchmd5_pl_seriale.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_benchmd5_pl_multithread.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_Handbrake_Test.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_autotest_7z.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_7z_Compression.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_lrzip_Compression.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_gzip_Compression.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_pigz_Compression.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_bzip2_Compression.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_lbzip2_Compression.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_pyrit.log"
safe_delete_file "${BENCHDIR}/tmp/mem_monitor_hardinfo.log"


if [ -e "${BENCHDIR}/tmp/bench.log" ]; then
    rm -f "${BENCHDIR}/tmp/bench.log"
fi
touch "${BENCHDIR}"/tmp/bench.log
#echo "esco da bench_createenvdir"
