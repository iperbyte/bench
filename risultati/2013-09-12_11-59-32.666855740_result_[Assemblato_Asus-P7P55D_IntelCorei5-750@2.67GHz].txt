Tempo per esecuzione in serie di 10 benchmd5.py                       (basso è meglio) :: @hasing_py_lin   = 00:01.41.438
Tempo per esecuzione in multithread di 100 benchmd5.py                (basso è meglio) :: @hasing_py_par   = 00:04.59.095
Tempo per esecuzione in serie di 10 benchmd5.pl                       (basso è meglio) :: @hasing_pl_lin   = 00:02.18.529
Tempo per esecuzione in multithread di 100 benchmd5.pl                (basso è meglio) :: @hasing_pl_par   = 00:07.10.234
HandBrakeCLI tempo per convertire ToS-4k-1920.mov in HANDB_x264.mp4   (basso è meglio) :: @handbrakeclim4v = 00:04.26.068
P7ZIP Compressione MB/Sec                                             (alto  è meglio) :: @b7zMBSCOMP      = 9201.50
P7ZIP Compressione MIBS per SingoloCore                               (alto  è meglio) :: @b7zVAL_AVRSC    = 3064
P7ZIP Compressione MIBS Complessivo su Tutti i Core                   (alto  è meglio) :: @b7zVAL_AVRMC    = 9686
P7ZIP DEcompressione MB/Sec                                           (alto  è meglio) :: @b7zMBSDECOMP    = 115281.25
P7ZIP DEcompressione MIBS per SingoloCore                             (alto  è meglio) :: @b7zVAL_AVRSD    = 2663
P7ZIP DEcompressione MIBS Complessivo su Tutti i Core                 (alto  è meglio) :: @b7zVAL_AVRMD    = 10619
P7ZIP Media Comp/Decomp MIBS per SingoloCore                          (alto  è meglio) :: @b7zVAL_TOTS     = 2863
P7ZIP Media Comp/Decomp MIBS Complessivo su Tutti i Core              (alto  è meglio) :: @b7zVAL_TOTM     = 10153
7zip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_z9.7z     (basso è meglio) :: @test7zip        = 00:01.41.568
lrzip:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920.lrz       (basso è meglio) :: @test_7zip       = 00:06.29.979
gzip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_gzip.gz   (basso è meglio) :: @test_gzip       = 00:01.04.248
pigz:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_pigz.gz   (basso è meglio) :: @test_pigz       = 00:00.50.175
bzip2:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_bzip2      = 00:03.24.780
lbzip2: tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_lbzip2     = 00:01.15.632
pyrit test PYRITPMKs                                                  (alto  è meglio) :: @PYRITPMKs       = 3068.78
Secondi test SYSBENCH su 4 Thread                                     (basso è meglio) :: @val_sysbench    = 20.1661s
HARDINFO TEST                                                                          :: @hinf_status     = PASS
Blowfish                                                              (basso è meglio) :: @hinf_Blowfish   = 2.959
CryptoHash                                                            (alto  è meglio) :: @hinf_CryptoHash = 411.374
Fibonacci                                                             (basso è meglio) :: @hinf_Fibonacci  = 2.295
NQueens                                                               (basso è meglio) :: @hinf_NQueens    = 5.892
FFT                                                                   (basso è meglio) :: @hinf_FFT        = 1.288
Raytracing                                                            (basso è meglio) :: @hinf_Raytracing = 5.667
 
 
HARDWARE:
 
 
Memoria utilizzata in Kb durante i test
Tutto OK, nessuna condizione di swap durante i test :: @mem_GLOBAL_SWAP_WARNING = PASS
Test sysbench            :: @mem_sysbench_STAT               = PASS
Test sysbench            :: @mem_sysbench_TOTUSED            = 32292
Test sysbench            :: @mem_sysbench_SWAPUSED           = 0
Test md5.py singlethread :: @mem_md5_single_py_STAT          = PASS
Test md5.py singlethread :: @mem_md5_single_py_TOTUSED       = 41484
Test md5.py singlethread :: @mem_md5_single_py_SWAPUSED      = 0
Test md5.py multithread  :: @mem_md5_multi_py_STAT           = PASS
Test md5.py multithread  :: @mem_md5_multi_py_TOTUSED        = 56408
Test md5.py multithread  :: @mem_md5_multi_py_SWAPUSED       = 0
Test md5.pl singlethread :: @mem_md5_single_pl_STAT          = PASS
Test md5.pl singlethread :: @mem_md5_single_pl_TOTUSED       = 26928
Test md5.pl singlethread :: @mem_md5_single_pl_SWAPUSED      = 0
Test md5.pl multithread  :: @mem_md5_multi_pl_STAT           = PASS
Test md5.pl multithread  :: @mem_md5_multi_pl_TOTUSED        = 44928
Test md5.pl multithread  :: @mem_md5_multi_pl_SWAPUSED       = 0
Test handbrake           :: @mem_handbrake_STAT              = PASS
Test handbrake           :: @mem_handbrake_TOTUSED           = 816800
Test handbrake           :: @mem_handbrake_SWAPUSED          = 0
Test 7z Autotest         :: @mem_7z_autotest_STAT            = PASS
Test 7z Autotest         :: @mem_7z_autotest_TOTUSED         = 839164
Test 7z Autotest         :: @mem_7z_autotest_SWAPUSED        = 0
Test 7z Compression      :: @mem_7z_compression_STAT         = PASS
Test 7z Compression      :: @mem_7z_compression_TOTUSED      = 2009792
Test 7z Compression      :: @mem_7z_compression_SWAPUSED     = 0
Test lrzip Compression   :: @mem_lrzip_compression_STAT      = PASS
Test lrzip Compression   :: @mem_lrzip_compression_TOTUSED   = 2421884
Test lrzip Compression   :: @mem_lrzip_compression_SWAPUSED  = 0
Test gzip Compression    :: @mem_gzip_compression_STAT       = PASS
Test gzip Compression    :: @mem_gzip_compression_TOTUSED    = 1464188
Test gzip Compression    :: @mem_gzip_compression_SWAPUSED   = 0
Test pigz Compression    :: @mem_pigz_compression_STAT       = PASS
Test pigz Compression    :: @mem_pigz_compression_TOTUSED    = 1467472
Test pigz Compression    :: @mem_pigz_compression_SWAPUSED   = 0
Test bzip2 Compression   :: @mem_bzip2_compression_STAT      = PASS
Test bzip2 Compression   :: @mem_bzip2_compression_TOTUSED   = 1473572
Test bzip2 Compression   :: @mem_bzip2_compression_SWAPUSED  = 0
Test lbzip2 Compression  :: @mem_lbzip2_compression_STAT     = PASS
Test lbzip2 Compression  :: @mem_lbzip2_compression_TOTUSED  = 1482400
Test lbzip2 Compression  :: @mem_lbzip2_compression_SWAPUSED = 0
Test Pyrit               :: @mem_pyrit_STAT                  = PASS
Test Pyrit               :: @mem_pyrit_TOTUSED               = 54900
Test Pyrit               :: @mem_pyrit_SWAPUSED              = 0
Test Hardinfo            :: @mem_hardinfo_STAT               = PASS
Test Hardinfo            :: @mem_hardinfo_TOTUSED            = 297136
Test Hardinfo            :: @mem_hardinfo_SWAPUSED           = 0

**************************** TURBOSTAT INFO ********************************
Turbostat General info [[[ @turbostatmemo
CPUID(0): GenuineIntel 11 CPUID levels; family:model:stepping 0x6:1e:5 (6:30:5)
CPUID(6): APERF, DTS
9 * 133 = 1200 MHz max efficiency
20 * 133 = 2667 MHz TSC frequency
21 * 133 = 2800 MHz max turbo 4 active cores
21 * 133 = 2800 MHz max turbo 3 active cores
24 * 133 = 3200 MHz max turbo 2 active cores
24 * 133 = 3200 MHz max turbo 1 active cores
]]]
Turbostat Valore minimo a riposo GHz                    :: @riposomin         = 1.20
Turbostat Valore minimo a riposo TSC                    :: @riposomintsc      = 2.68
Turbostat Valore massimo con 1 thread al lavoro GHz     :: @stress1coremax    = 3.21
Turbostat Valore massimo con 1 thread al lavoro TSC     :: @stress1coremaxtsc = 3.21
Turbostat Valore massimo con tutti thread al lavoro GHz :: @stressfullcoremax = 2.81
Turbostat Valore massimo con tutti thread al lavoro TSC :: @stressfullcoremax = 2.81
 
********************************************* CPU INFO *********************************************
Modello CPU       :: @CPU_NAME         = Intel(R) Core(TM) i5 CPU 750 @ 2.67GHz
Desc. aggiuntiva  :: @CPU_DESC         = 1 dual-core processor with hyper-threading (2 threads per core)
Identificativo    :: @CPU_ID           = E5 06 01 00 FF FB EB BF
soket             :: @CPU_SCK          = LGA1156
Num. Core fisici  :: @CPU_NUMCORE      = 4
Thread per core   :: @CPU_THREADxCORE  = 1
Tot Thread logici :: @CPU_THREAD       = 4
Voltaggio CPU     :: @CPU_VOLT         = unknown
CPU Ext FAMILY    :: @CPU_EFAMILY      = 0
CPU Ext MODEL     :: @CPU_EMODEL       = 1
CPU FAMILY        :: @CPU_FAMILY       = 6
CPU MODEL         :: @CPU_MODEL        = 30
CPU STEPPING      :: @CPU_STEPPING     = 5
MICROCODE         :: @CPU_MICROCODE    = 0x3
MHZ               :: @CPU_MHZ          = 1200.000
MAXMHZ            :: @CPU_MAXMHZ       = 3800 MHz
External CLOCK    :: @CPU_EXTCLOCK     = 133 MHz
CACHE L1D         :: @CPU_L1DCACHE     = 32K
CACHE L1I         :: @CPU_L1ICACHE     = 32K
CACHE L2          :: @CPU_L2CACHE      = 256K
CACHE L3          :: @CPU_L3CACHE      = 8192K
Virtualiz. Tech.  :: @CPU_VTX          = VT-x
Cooproc.Mat. (FPU):: @FPU_PRESENT      = yes
FPU EXCEPTION     :: @FPU_EXCEPTION    = yes
WRITE PROTECT     :: @CPU_WRITEPROTECT = yes
LONG NOPS         :: @CPU_LONG_NOPS    = yes
Address mode      :: @CPU_ADDR         = 36 bits physical, 48 bits virtual
Power Manager     :: @CPU_POWERMANAGER = unknown
FLAGS             :: @CPU_FLAGS        = fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni dtes64 monitor ds_cpl vmx smx est tm2 ssse3 cx16 xtpr pdcm sse4_1 sse4_2 popcnt lahf_lm ida dtherm tpr_shadow vnmi flexpriority ept vpid

********************************************* RAM INFO *********************************************
Memoria Totale in kb        :: @mem_total         = 4039104
Memoria disponibile in kb   :: @mem_avaiable      = 4039104
Memoria utilizzata in kb    :: @mem_used          = 725716
Memoria libera in kb        :: @mem_free          = 3313120
Swap disponibile in kb      :: @swap_avaiable     = 8295420
Swap utilizzata in kb       :: @swap_used         = 0
Swap libera in kb           :: @swap_free         = 8295420
Totale benchi di memoria    :: @Tot_Banchi        = 2

Nome Slot del banco         :: @mem_bankpos_1     = DIMM0
Dimensione del banco        :: @mem_banksize_1    = 2048 MB
Tipo di memoria             :: @mem_banktype_1    = DDR
Velocità del banco          :: @mem_bankspeed_1   = 1333 MHz
Marca                       :: @mem_manufacture_1 = Manufacturer0
Modello                     :: @mem_model_1       = PartNum0
Velocità di clock impostata :: @mem_clock_1       =  

Nome Slot del banco         :: @mem_bankpos_2     = DIMM2
Dimensione del banco        :: @mem_banksize_2    = 2048 MB
Tipo di memoria             :: @mem_banktype_2    = DDR
Velocità del banco          :: @mem_bankspeed_2   = 1333 MHz
Marca                       :: @mem_manufacture_2 = Manufacturer2
Modello                     :: @mem_model_2       = PartNum2
Velocità di clock impostata :: @mem_clock_2       =  

********************************************* VGA INFO *********************************************
Scheda video       :: @val_SVproduct              = G92 [GeForce 9800 GT]
Versione           :: @val_SVversion              = a2
Produttore         :: @val_SVvendor               = Gigabyte Technology Co., Ltd Device 34d3
Produttore chipset :: @val_SVchipsetVendor        = NVIDIA Corporation
Ampiezza di bus    :: @val_SVbusWidth             = 64 bits
Velocità di clock  :: @val_SVclock                = 33MHz
Capacità           :: @val_SVcapabilities         = pm msi pciexpress vga_controller bus_master cap_list rom
Flags              :: @val_SVflags                = bus master, fast devsel, latency 0, IRQ 16
Driver utilizzati  :: @val_SVdriver               = nouveau
Modulo kernel      :: @val_SVmodule               = 
Versione OpenGL    :: @val_SVopenglversion        = 3.0 Mesa 9.1.3
Info OpenGL Vendor :: @val_SVopenglvendorstring   = nouveau
OpenGL renderer    :: @val_SVopenglrendererstring = Gallium 0.4 on NV92
[[[ @infoMEMVGAmemo
	Memory at fa000000 (32-bit, non-prefetchable) [size=16M]
	Memory at d0000000 (64-bit, prefetchable) [size=256M]
	Memory at f8000000 (64-bit, non-prefetchable) [size=32M]
]]]

********************************************* BOARD INFO *********************************************
Marca   della scheda madre   :: @MB_manufacturer = ASUSTeK Computer INC.
modello della scheda madre   :: @MB_model        = P7P55D
revisione della scheda madre :: @MB_revision     = Rev 1.xx
Bios                         :: @BIOS_vendor     = American Megatrends Inc.
Versione del bios            :: @BIOS_version    = 0501
Data del bios                :: @BIOS_date       = 08/21/2009

********************************************* xBUNTU INFO *********************************************
Distribuzione linux          :: @LinuxDistroName = Ubuntu 13.04
Kernel                       :: @LinuxKenelInfo  = Linux 3.8.0-29-generic x86_64 GNU/Linux
 
 
SEQUENCE TEST VERSION - :: @SEQUENCE_TEST_VERSION = 0.0.prealfa
OUTPUT VERSION        - :: @OUTPUT_VERSION = 0.3.prealfa
ID TEST               - :: @UUID_ID_TEST = 29e6827d-f1c4-4842-9d91-53d535876437
