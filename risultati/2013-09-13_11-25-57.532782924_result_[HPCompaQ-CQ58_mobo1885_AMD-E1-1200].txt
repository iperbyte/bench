Tempo per esecuzione in serie di 10 benchmd5.py                       (basso è meglio) :: @hasing_py_lin   = 00:05.40.432
Tempo per esecuzione in multithread di 100 benchmd5.py                (basso è meglio) :: @hasing_py_par   = 00:28.47.624
Tempo per esecuzione in serie di 10 benchmd5.pl                       (basso è meglio) :: @hasing_pl_lin   = 00:07.51.356
Tempo per esecuzione in multithread di 100 benchmd5.pl                (basso è meglio) :: @hasing_pl_par   = 00:40.56.299
HandBrakeCLI tempo per convertire ToS-4k-1920.mov in HANDB_x264.mp4   (basso è meglio) :: @handbrakeclim4v = 00:48.09.966
P7ZIP Compressione MB/Sec                                             (alto  è meglio) :: @b7zMBSCOMP      = 1579.50
P7ZIP Compressione MIBS per SingoloCore                               (alto  è meglio) :: @b7zVAL_AVRSC    = 924
P7ZIP Compressione MIBS Complessivo su Tutti i Core                   (alto  è meglio) :: @b7zVAL_AVRMC    = 1665
P7ZIP DEcompressione MB/Sec                                           (alto  è meglio) :: @b7zMBSDECOMP    = 25676.50
P7ZIP DEcompressione MIBS per SingoloCore                             (alto  è meglio) :: @b7zVAL_AVRSD    = 1189
P7ZIP DEcompressione MIBS Complessivo su Tutti i Core                 (alto  è meglio) :: @b7zVAL_AVRMD    = 2366
P7ZIP Media Comp/Decomp MIBS per SingoloCore                          (alto  è meglio) :: @b7zVAL_TOTS     = 1057
P7ZIP Media Comp/Decomp MIBS Complessivo su Tutti i Core              (alto  è meglio) :: @b7zVAL_TOTM     = 2015
7zip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_z9.7z     (basso è meglio) :: @test7zip        = 00:09.44.065
lrzip:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920.lrz       (basso è meglio) :: @test_7zip       = 00:23.18.556
gzip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_gzip.gz   (basso è meglio) :: @test_gzip       = 00:02.44.708
pigz:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_pigz.gz   (basso è meglio) :: @test_pigz       = 00:01.42.548
bzip2:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_bzip2      = 00:10.15.367
lbzip2: tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_lbzip2     = 00:04.01.706
pyrit test PYRITPMKs                                                  (alto  è meglio) :: @PYRITPMKs       = 264.48
Secondi test SYSBENCH su 2 Thread                                     (basso è meglio) :: @val_sysbench    = 311.5173s
HARDINFO TEST                                                                          :: @hinf_status     = PASS
Blowfish                                                              (basso è meglio) :: @hinf_Blowfish   = 16.280
CryptoHash                                                            (alto  è meglio) :: @hinf_CryptoHash = 70.855
Fibonacci                                                             (basso è meglio) :: @hinf_Fibonacci  = 6.141
NQueens                                                               (basso è meglio) :: @hinf_NQueens    = 23.683
FFT                                                                   (basso è meglio) :: @hinf_FFT        = 9.062
Raytracing                                                            (basso è meglio) :: @hinf_Raytracing = 20.058
 
 
HARDWARE:
 
 
Memoria utilizzata in Kb durante i test
Tutto OK, nessuna condizione di swap durante i test :: @mem_GLOBAL_SWAP_WARNING = PASS
Test sysbench            :: @mem_sysbench_STAT               = PASS
Test sysbench            :: @mem_sysbench_TOTUSED            = 36868
Test sysbench            :: @mem_sysbench_SWAPUSED           = 0
Test md5.py singlethread :: @mem_md5_single_py_STAT          = PASS
Test md5.py singlethread :: @mem_md5_single_py_TOTUSED       = 32316
Test md5.py singlethread :: @mem_md5_single_py_SWAPUSED      = 0
Test md5.py multithread  :: @mem_md5_multi_py_STAT           = PASS
Test md5.py multithread  :: @mem_md5_multi_py_TOTUSED        = 108520
Test md5.py multithread  :: @mem_md5_multi_py_SWAPUSED       = 0
Test md5.pl singlethread :: @mem_md5_single_pl_STAT          = PASS
Test md5.pl singlethread :: @mem_md5_single_pl_TOTUSED       = 83484
Test md5.pl singlethread :: @mem_md5_single_pl_SWAPUSED      = 0
Test md5.pl multithread  :: @mem_md5_multi_pl_STAT           = PASS
Test md5.pl multithread  :: @mem_md5_multi_pl_TOTUSED        = 100432
Test md5.pl multithread  :: @mem_md5_multi_pl_SWAPUSED       = 0
Test handbrake           :: @mem_handbrake_STAT              = PASS
Test handbrake           :: @mem_handbrake_TOTUSED           = 984400
Test handbrake           :: @mem_handbrake_SWAPUSED          = 0
Test 7z Autotest         :: @mem_7z_autotest_STAT            = PASS
Test 7z Autotest         :: @mem_7z_autotest_TOTUSED         = 437068
Test 7z Autotest         :: @mem_7z_autotest_SWAPUSED        = 0
Test 7z Compression      :: @mem_7z_compression_STAT         = PASS
Test 7z Compression      :: @mem_7z_compression_TOTUSED      = 1688728
Test 7z Compression      :: @mem_7z_compression_SWAPUSED     = 0
Test lrzip Compression   :: @mem_lrzip_compression_STAT      = PASS
Test lrzip Compression   :: @mem_lrzip_compression_TOTUSED   = 2360452
Test lrzip Compression   :: @mem_lrzip_compression_SWAPUSED  = 0
Test gzip Compression    :: @mem_gzip_compression_STAT       = PASS
Test gzip Compression    :: @mem_gzip_compression_TOTUSED    = 1470532
Test gzip Compression    :: @mem_gzip_compression_SWAPUSED   = 0
Test pigz Compression    :: @mem_pigz_compression_STAT       = PASS
Test pigz Compression    :: @mem_pigz_compression_TOTUSED    = 1466504
Test pigz Compression    :: @mem_pigz_compression_SWAPUSED   = 0
Test bzip2 Compression   :: @mem_bzip2_compression_STAT      = PASS
Test bzip2 Compression   :: @mem_bzip2_compression_TOTUSED   = 1474760
Test bzip2 Compression   :: @mem_bzip2_compression_SWAPUSED  = 0
Test lbzip2 Compression  :: @mem_lbzip2_compression_STAT     = PASS
Test lbzip2 Compression  :: @mem_lbzip2_compression_TOTUSED  = 1482464
Test lbzip2 Compression  :: @mem_lbzip2_compression_SWAPUSED = 0
Test Pyrit               :: @mem_pyrit_STAT                  = PASS
Test Pyrit               :: @mem_pyrit_TOTUSED               = 103500
Test Pyrit               :: @mem_pyrit_SWAPUSED              = 0
Test Hardinfo            :: @mem_hardinfo_STAT               = PASS
Test Hardinfo            :: @mem_hardinfo_TOTUSED            = 77996
Test Hardinfo            :: @mem_hardinfo_SWAPUSED           = 0

**************************** TURBOSTAT INFO ********************************
Turbostat General info [[[ @turbostatmemo
CPUID(0): AuthenticAMD 6 CPUID levels; family:model:stepping 0xf:2:0 (15:2:0)
CPUID(6): APERF
]]]
Turbostat Valore minimo a riposo GHz                    :: @riposomin         = 0.78
Turbostat Valore minimo a riposo TSC                    :: @riposomintsc      = 1.40
Turbostat Valore massimo con 1 thread al lavoro GHz     :: @stress1coremax    = 1.40
Turbostat Valore massimo con 1 thread al lavoro TSC     :: @stress1coremaxtsc = 1.40
Turbostat Valore massimo con tutti thread al lavoro GHz :: @stressfullcoremax = 1.40
Turbostat Valore massimo con tutti thread al lavoro TSC :: @stressfullcoremax = 1.40
 
********************************************* CPU INFO *********************************************
Modello CPU       :: @CPU_NAME         = AMD E1-1200 APU with Radeon(tm) HD Graphics
Desc. aggiuntiva  :: @CPU_DESC         = unknown
Identificativo    :: @CPU_ID           = 20 0F 50 00 FF FB 8B 17
soket             :: @CPU_SCK          = Socket FT1
Num. Core fisici  :: @CPU_NUMCORE      = 2
Thread per core   :: @CPU_THREADxCORE  = 1
Tot Thread logici :: @CPU_THREAD       = 2
Voltaggio CPU     :: @CPU_VOLT         = unknown
CPU Ext FAMILY    :: @CPU_EFAMILY      = 5
CPU Ext MODEL     :: @CPU_EMODEL       = 0
CPU FAMILY        :: @CPU_FAMILY       = 20
CPU MODEL         :: @CPU_MODEL        = 2
CPU STEPPING      :: @CPU_STEPPING     = 0
MICROCODE         :: @CPU_MICROCODE    = 0x500010d
MHZ               :: @CPU_MHZ          = 1400.000
MAXMHZ            :: @CPU_MAXMHZ       = 1400 MHz
External CLOCK    :: @CPU_EXTCLOCK     = 100 MHz
CACHE L1D         :: @CPU_L1DCACHE     = 32K
CACHE L1I         :: @CPU_L1ICACHE     = 32K
CACHE L2          :: @CPU_L2CACHE      = 512K
CACHE L3          :: @CPU_L3CACHE      = unknown
Virtualiz. Tech.  :: @CPU_VTX          = AMD-V
Cooproc.Mat. (FPU):: @FPU_PRESENT      = yes
FPU EXCEPTION     :: @FPU_EXCEPTION    = yes
WRITE PROTECT     :: @CPU_WRITEPROTECT = yes
LONG NOPS         :: @CPU_LONG_NOPS    = yes
Address mode      :: @CPU_ADDR         = 36 bits physical, 48 bits virtual
Power Manager     :: @CPU_POWERMANAGER = ts ttp tm stc 100mhzsteps hwpstate
FLAGS             :: @CPU_FLAGS        = fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx mmxext fxsr_opt pdpe1gb rdtscp lm constant_tsc rep_good nopl nonstop_tsc extd_apicid aperfmperf pni monitor ssse3 cx16 popcnt lahf_lm cmp_legacy svm extapic cr8_legacy abm sse4a misalignsse 3dnowprefetch ibs skinit wdt arat hw_pstate npt lbrv svm_lock nrip_save pausefilter

********************************************* RAM INFO *********************************************
Memoria Totale in kb        :: @mem_total         = 3638376
Memoria disponibile in kb   :: @mem_avaiable      = 3638376
Memoria utilizzata in kb    :: @mem_used          = 539784
Memoria libera in kb        :: @mem_free          = 3098692
Swap disponibile in kb      :: @swap_avaiable     = 8295420
Swap utilizzata in kb       :: @swap_used         = 0
Swap libera in kb           :: @swap_free         = 8295420
Totale benchi di memoria    :: @Tot_Banchi        = 1

Nome Slot del banco         :: @mem_bankpos_1     = Bottom-Slot2(under)
Dimensione del banco        :: @mem_banksize_1    = 4096 MB
Tipo di memoria             :: @mem_banktype_1    = DDR3
Velocità del banco          :: @mem_bankspeed_1   = 1066 MHz
Marca                       :: @mem_manufacture_1 = A-DATATechnology
Modello                     :: @mem_model_1       = AM1U16BC4P2-B19H
Velocità di clock impostata :: @mem_clock_1       = 1066 MHz

********************************************* VGA INFO *********************************************
Scheda video       :: @val_SVproduct              = Wrestler [Radeon HD 7310]
Versione           :: @val_SVversion              = 00
Produttore         :: @val_SVvendor               = Hewlett-Packard Company Device 1885
Produttore chipset :: @val_SVchipsetVendor        = Advanced Micro Devices [AMD] nee ATI
Ampiezza di bus    :: @val_SVbusWidth             = 32 bits
Velocità di clock  :: @val_SVclock                = 33MHz
Capacità           :: @val_SVcapabilities         = pm pciexpress msi vga_controller bus_master cap_list rom
Flags              :: @val_SVflags                = bus master, fast devsel, latency 0, IRQ 43
Driver utilizzati  :: @val_SVdriver               = radeon
Modulo kernel      :: @val_SVmodule               = 
Versione OpenGL    :: @val_SVopenglversion        = 3.0 Mesa 9.1.3
Info OpenGL Vendor :: @val_SVopenglvendorstring   = X.Org
OpenGL renderer    :: @val_SVopenglrendererstring = Gallium 0.4 on AMD PALM
[[[ @infoMEMVGAmemo
	Memory at e0000000 (32-bit, prefetchable) [size=256M]
	Memory at f0300000 (32-bit, non-prefetchable) [size=256K]
]]]

********************************************* BOARD INFO *********************************************
Marca   della scheda madre   :: @MB_manufacturer = Hewlett-Packard
modello della scheda madre   :: @MB_model        = 1885
revisione della scheda madre :: @MB_revision     = 66.33
Bios                         :: @BIOS_vendor     = Insyde
Versione del bios            :: @BIOS_version    = F.23
Data del bios                :: @BIOS_date       = 11/17/2012

********************************************* xBUNTU INFO *********************************************
Distribuzione linux          :: @LinuxDistroName = Ubuntu 13.04
Kernel                       :: @LinuxKenelInfo  = Linux 3.8.0-29-generic x86_64 GNU/Linux
 
 
SEQUENCE TEST VERSION - :: @SEQUENCE_TEST_VERSION = 0.0.prealfa
OUTPUT VERSION        - :: @OUTPUT_VERSION = 0.3.prealfa
ID TEST               - :: @UUID_ID_TEST = beec1b64-0f79-42cb-9d8f-c27f8855d79f
