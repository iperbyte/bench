Tempo per esecuzione in serie di 10 benchmd5.py                       (basso è meglio) :: @hasing_py_lin   = 00:01.16.241
Tempo per esecuzione in multithread di 100 benchmd5.py                (basso è meglio) :: @hasing_py_par   = 00:06.43.623
Tempo per esecuzione in serie di 10 benchmd5.pl                       (basso è meglio) :: @hasing_pl_lin   = 00:01.48.497
Tempo per esecuzione in multithread di 100 benchmd5.pl                (basso è meglio) :: @hasing_pl_par   = 00:09.42.724
HandBrakeCLI tempo per convertire ToS-4k-1920.mov in HANDB_x264.mp4   (basso è meglio) :: @handbrakeclim4v = 00:05.01.621
P7ZIP Compressione MB/Sec                                             (alto  è meglio) :: @b7zMBSCOMP      = 8993.00
P7ZIP Compressione MIBS per SingoloCore                               (alto  è meglio) :: @b7zVAL_AVRSC    = 2859
P7ZIP Compressione MIBS Complessivo su Tutti i Core                   (alto  è meglio) :: @b7zVAL_AVRMC    = 9490
P7ZIP DEcompressione MB/Sec                                           (alto  è meglio) :: @b7zMBSDECOMP    = 109468.25
P7ZIP DEcompressione MIBS per SingoloCore                             (alto  è meglio) :: @b7zVAL_AVRSD    = 2531
P7ZIP DEcompressione MIBS Complessivo su Tutti i Core                 (alto  è meglio) :: @b7zVAL_AVRMD    = 10084
P7ZIP Media Comp/Decomp MIBS per SingoloCore                          (alto  è meglio) :: @b7zVAL_TOTS     = 2695
P7ZIP Media Comp/Decomp MIBS Complessivo su Tutti i Core              (alto  è meglio) :: @b7zVAL_TOTM     = 9787
7zip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_z9.7z     (basso è meglio) :: @test7zip        = 00:01.45.377
lrzip:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920.lrz       (basso è meglio) :: @test_7zip       = 00:02.10.218
gzip:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_gzip.gz   (basso è meglio) :: @test_gzip       = 00:01.14.253
pigz:   tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_pigz.gz   (basso è meglio) :: @test_pigz       = 00:00.52.303
bzip2:  tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_bzip2      = 00:02.37.641
lbzip2: tempo per comprimere ToS-4k-1920.mov in ToS-4k-1920_bzip2.gz  (basso è meglio) :: @test_lbzip2     = 00:01.17.398
pyrit test PYRITPMKs                                                  (alto  è meglio) :: @PYRITPMKs       = 2301.55
Secondi test SYSBENCH su 4 Thread                                     (basso è meglio) :: @val_sysbench    = 25.0341s
HARDINFO TEST                                                                          :: @hinf_status     = PASS
Blowfish                                                              (basso è meglio) :: @hinf_Blowfish   = 3.000
CryptoHash                                                            (alto  è meglio) :: @hinf_CryptoHash = 472.545
Fibonacci                                                             (basso è meglio) :: @hinf_Fibonacci  = 1.339
NQueens                                                               (basso è meglio) :: @hinf_NQueens    = 3.487
FFT                                                                   (basso è meglio) :: @hinf_FFT        = 0.843
Raytracing                                                            (basso è meglio) :: @hinf_Raytracing = 4.394
 
 
HARDWARE:
 
 
Memoria utilizzata in Kb durante i test
Tutto OK, nessuna condizione di swap durante i test :: @mem_GLOBAL_SWAP_WARNING = PASS
Test sysbench            :: @mem_sysbench_STAT               = PASS
Test sysbench            :: @mem_sysbench_TOTUSED            = 38188
Test sysbench            :: @mem_sysbench_SWAPUSED           = 0
Test md5.py singlethread :: @mem_md5_single_py_STAT          = PASS
Test md5.py singlethread :: @mem_md5_single_py_TOTUSED       = 31096
Test md5.py singlethread :: @mem_md5_single_py_SWAPUSED      = 0
Test md5.py multithread  :: @mem_md5_multi_py_STAT           = PASS
Test md5.py multithread  :: @mem_md5_multi_py_TOTUSED        = 56364
Test md5.py multithread  :: @mem_md5_multi_py_SWAPUSED       = 0
Test md5.pl singlethread :: @mem_md5_single_pl_STAT          = PASS
Test md5.pl singlethread :: @mem_md5_single_pl_TOTUSED       = 27048
Test md5.pl singlethread :: @mem_md5_single_pl_SWAPUSED      = 0
Test md5.pl multithread  :: @mem_md5_multi_pl_STAT           = PASS
Test md5.pl multithread  :: @mem_md5_multi_pl_TOTUSED        = 60480
Test md5.pl multithread  :: @mem_md5_multi_pl_SWAPUSED       = 0
Test handbrake           :: @mem_handbrake_STAT              = PASS
Test handbrake           :: @mem_handbrake_TOTUSED           = 741824
Test handbrake           :: @mem_handbrake_SWAPUSED          = 0
Test 7z Autotest         :: @mem_7z_autotest_STAT            = PASS
Test 7z Autotest         :: @mem_7z_autotest_TOTUSED         = 828580
Test 7z Autotest         :: @mem_7z_autotest_SWAPUSED        = 0
Test 7z Compression      :: @mem_7z_compression_STAT         = PASS
Test 7z Compression      :: @mem_7z_compression_TOTUSED      = 2126056
Test 7z Compression      :: @mem_7z_compression_SWAPUSED     = 0
Test lrzip Compression   :: @mem_lrzip_compression_STAT      = PASS
Test lrzip Compression   :: @mem_lrzip_compression_TOTUSED   = 2896516
Test lrzip Compression   :: @mem_lrzip_compression_SWAPUSED  = 0
Test gzip Compression    :: @mem_gzip_compression_STAT       = PASS
Test gzip Compression    :: @mem_gzip_compression_TOTUSED    = 1461048
Test gzip Compression    :: @mem_gzip_compression_SWAPUSED   = 0
Test pigz Compression    :: @mem_pigz_compression_STAT       = PASS
Test pigz Compression    :: @mem_pigz_compression_TOTUSED    = 1462896
Test pigz Compression    :: @mem_pigz_compression_SWAPUSED   = 0
Test bzip2 Compression   :: @mem_bzip2_compression_STAT      = PASS
Test bzip2 Compression   :: @mem_bzip2_compression_TOTUSED   = 1467244
Test bzip2 Compression   :: @mem_bzip2_compression_SWAPUSED  = 0
Test lbzip2 Compression  :: @mem_lbzip2_compression_STAT     = PASS
Test lbzip2 Compression  :: @mem_lbzip2_compression_TOTUSED  = 1480700
Test lbzip2 Compression  :: @mem_lbzip2_compression_SWAPUSED = 0
Test Pyrit               :: @mem_pyrit_STAT                  = PASS
Test Pyrit               :: @mem_pyrit_TOTUSED               = 62048
Test Pyrit               :: @mem_pyrit_SWAPUSED              = 0
Test Hardinfo            :: @mem_hardinfo_STAT               = PASS
Test Hardinfo            :: @mem_hardinfo_TOTUSED            = 292072
Test Hardinfo            :: @mem_hardinfo_SWAPUSED           = 0

**************************** TURBOSTAT INFO ********************************
Turbostat General info [[[ @turbostatmemo
CPUID(0): GenuineIntel 13 CPUID levels; family:model:stepping 0x6:3a:9 (6:58:9)
CPUID(6): APERF, DTS, PTM, EPB
16 * 100 = 1600 MHz max efficiency
34 * 100 = 3400 MHz TSC frequency
34 * 100 = 3400 MHz max turbo 4 active cores
34 * 100 = 3400 MHz max turbo 3 active cores
34 * 100 = 3400 MHz max turbo 2 active cores
34 * 100 = 3400 MHz max turbo 1 active cores
cpu0: PKG Limit #2: ENabled (68.750000 Watts, 0.000977* sec, clamp DISabled)
]]]
Turbostat Valore minimo a riposo GHz                    :: @riposomin         = 1.60
Turbostat Valore minimo a riposo TSC                    :: @riposomintsc      = 3.41
Turbostat Valore massimo con 1 thread al lavoro GHz     :: @stress1coremax    = 3.41
Turbostat Valore massimo con 1 thread al lavoro TSC     :: @stress1coremaxtsc = 3.41
Turbostat Valore massimo con tutti thread al lavoro GHz :: @stressfullcoremax = 3.41
Turbostat Valore massimo con tutti thread al lavoro TSC :: @stressfullcoremax = 3.41
 
********************************************* CPU INFO *********************************************
Modello CPU       :: @CPU_NAME         = Intel(R) Core(TM) i3-3240 CPU @ 3.40GHz
Desc. aggiuntiva  :: @CPU_DESC         = 1 dual-core processor with hyper-threading (2 threads per core)
Identificativo    :: @CPU_ID           = A9 06 03 00 FF FB EB BF
soket             :: @CPU_SCK          = LGA1155
Num. Core fisici  :: @CPU_NUMCORE      = 2
Thread per core   :: @CPU_THREADxCORE  = 2
Tot Thread logici :: @CPU_THREAD       = 4
Voltaggio CPU     :: @CPU_VOLT         = unknown
CPU Ext FAMILY    :: @CPU_EFAMILY      = 0
CPU Ext MODEL     :: @CPU_EMODEL       = 3
CPU FAMILY        :: @CPU_FAMILY       = 6
CPU MODEL         :: @CPU_MODEL        = 58
CPU STEPPING      :: @CPU_STEPPING     = 9
MICROCODE         :: @CPU_MICROCODE    = 0x12
MHZ               :: @CPU_MHZ          = 3400.000
MAXMHZ            :: @CPU_MAXMHZ       = 3800 MHz
External CLOCK    :: @CPU_EXTCLOCK     = 100 MHz
CACHE L1D         :: @CPU_L1DCACHE     = 32K
CACHE L1I         :: @CPU_L1ICACHE     = 32K
CACHE L2          :: @CPU_L2CACHE      = 256K
CACHE L3          :: @CPU_L3CACHE      = 3072K
Virtualiz. Tech.  :: @CPU_VTX          = VT-x
Cooproc.Mat. (FPU):: @FPU_PRESENT      = yes
FPU EXCEPTION     :: @FPU_EXCEPTION    = yes
WRITE PROTECT     :: @CPU_WRITEPROTECT = yes
LONG NOPS         :: @CPU_LONG_NOPS    = yes
Address mode      :: @CPU_ADDR         = 36 bits physical, 48 bits virtual
Power Manager     :: @CPU_POWERMANAGER = unknown
FLAGS             :: @CPU_FLAGS        = fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 popcnt tsc_deadline_timer xsave avx f16c lahf_lm arat epb xsaveopt pln pts dtherm tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms

********************************************* RAM INFO *********************************************
Memoria Totale in kb        :: @mem_total         = 7866880
Memoria disponibile in kb   :: @mem_avaiable      = 7866880
Memoria utilizzata in kb    :: @mem_used          = 955264
Memoria libera in kb        :: @mem_free          = 6911496
Swap disponibile in kb      :: @swap_avaiable     = 8295420
Swap utilizzata in kb       :: @swap_used         = 0
Swap libera in kb           :: @swap_free         = 8295420
Totale benchi di memoria    :: @Tot_Banchi        = 2

Nome Slot del banco         :: @mem_bankpos_1     = ChannelA-DIMM0
Dimensione del banco        :: @mem_banksize_1    = 4096 MB
Tipo di memoria             :: @mem_banktype_1    = DDR3
Velocità del banco          :: @mem_bankspeed_1   = 1333 MHz
Marca                       :: @mem_manufacture_1 = Kingston
Modello                     :: @mem_model_1       = 9905471-006.A01LF
Velocità di clock impostata :: @mem_clock_1       = 1333 MHz

Nome Slot del banco         :: @mem_bankpos_2     = ChannelA-DIMM1
Dimensione del banco        :: @mem_banksize_2    = 4096 MB
Tipo di memoria             :: @mem_banktype_2    = DDR3
Velocità del banco          :: @mem_bankspeed_2   = 1333 MHz
Marca                       :: @mem_manufacture_2 = Kingston
Modello                     :: @mem_model_2       = 9905471-006.A01LF
Velocità di clock impostata :: @mem_clock_2       = 1333 MHz

********************************************* VGA INFO *********************************************
Scheda video       :: @val_SVproduct              = Xeon E3-1200 v2/3rd Gen Core processor Graphics Controller
Versione           :: @val_SVversion              = 09
Produttore         :: @val_SVvendor               = ASUSTeK Computer Inc. P8H77-I Motherboard
Produttore chipset :: @val_SVchipsetVendor        = Intel Corporation
Ampiezza di bus    :: @val_SVbusWidth             = 64 bits
Velocità di clock  :: @val_SVclock                = 33MHz
Capacità           :: @val_SVcapabilities         = msi pm vga_controller bus_master cap_list rom
Flags              :: @val_SVflags                = bus master, fast devsel, latency 0, IRQ 44
Driver utilizzati  :: @val_SVdriver               = i915
Modulo kernel      :: @val_SVmodule               = 
Versione OpenGL    :: @val_SVopenglversion        = 3.0 Mesa 9.1.3
Info OpenGL Vendor :: @val_SVopenglvendorstring   = Intel Open Source Technology Center
OpenGL renderer    :: @val_SVopenglrendererstring = Mesa DRI Intel(R) Ivybridge Desktop 
[[[ @infoMEMVGAmemo
	Memory at f7800000 (64-bit, non-prefetchable) [size=4M]
	Memory at e0000000 (64-bit, prefetchable) [size=256M]
]]]

********************************************* BOARD INFO *********************************************
Marca   della scheda madre   :: @MB_manufacturer = ASUSTeK COMPUTER INC.
modello della scheda madre   :: @MB_model        = P8Z77-V LX
revisione della scheda madre :: @MB_revision     = Rev X.0x
Bios                         :: @BIOS_vendor     = American Megatrends Inc.
Versione del bios            :: @BIOS_version    = 0610
Data del bios                :: @BIOS_date       = 05/08/2012

********************************************* xBUNTU INFO *********************************************
Distribuzione linux          :: @LinuxDistroName = Ubuntu 13.04
Kernel                       :: @LinuxKenelInfo  = Linux 3.8.0-29-generic x86_64 GNU/Linux
 
 
SEQUENCE TEST VERSION - :: @SEQUENCE_TEST_VERSION = 0.0.prealfa
OUTPUT VERSION        - :: @OUTPUT_VERSION = 0.3.prealfa
ID TEST               - :: @UUID_ID_TEST = 7e9b36e1-8be0-4ab8-a652-accd1291048f
